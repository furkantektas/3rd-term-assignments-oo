/*
 * File:    111044029HW08File.h
 *
 * Course:  CSE241
 * Project: HW08
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 10, 2012, 06:29 PM
 */
 
#ifndef FILE_H
	#include "111044029HW08File.h"
#endif

namespace Tektas{
	//getters
	string File::getPathName() const {
		return _pathName;
	}

	// alias for Document's getText() function
	string File::getContent() const {
		return Document::getText();
	}

	//setters
	void File::setPathName(string pathName) {
		_pathName = pathName;
	}

	// alias for Document's setText() function
	void File::setContent(string content) {
		Document::setText(content);
	}

	File& File::operator=(const File& other) {
		_pathName = other._pathName;
		Document::operator=(other);
	    return *this;
	}
}
