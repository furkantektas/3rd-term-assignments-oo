/*
 * File:    111044029HW08Document.h
 *
 * Course:  CSE241
 * Project: HW08
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 10, 2012, 06:29 PM
 */

#ifndef DOCUMENT_H
 	#define DOCUMENT_H

#include <string>
using std::string;

namespace Tektas{

	class Document {
		public:
			Document(string text = "") :
			         _text(text)
			         { /* Deliberately left blank.*/ };

			string getText() const;
			void setText(string text);
			Document& operator=(const Document& other);

		private:
			string _text;
	};

}

 #endif // DOCUMENT_H