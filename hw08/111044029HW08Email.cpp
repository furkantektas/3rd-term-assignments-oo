/*
 * File:    111044029HW08Email.cpp
 *
 * Course:  CSE241
 * Project: HW08
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 10, 2012, 06:29 PM
 */
 
#ifndef EMAIL_H
	#include "111044029HW08Email.h"
#endif

namespace Tektas{
	
	//getters
	string Email::getSender() const {
		return _sender;
	}

	string Email::getRecipient() const {
		return _recipient;
	}

	string Email::getTitle() const {
		return _title;
	}

	// alias for Document's getText() function
	string Email::getBody() const {
		return Document::getText();
	}

	//setters
	void Email::setSender(string sender) {
		_sender = sender;
	}

	void Email::setRecipient(string recipient) {
		_recipient = recipient;
	}

	void Email::setTitle(string title) {
		_title = title;
	}

	// alias for Document's setText() function
	void Email::setBody(string body) {
		Document::setText(body);
	}

	Email& Email::operator=(const Email& other) {
		_sender = other._sender;
		_recipient = other._recipient;
		_sender = other._sender;
		Document::operator=(other);
	    return *this;
	}
}