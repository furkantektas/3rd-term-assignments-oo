/*
 * File:    111044029HW08File.h
 *
 * Course:  CSE241
 * Project: HW08
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 10, 2012, 06:29 PM
 */
 
 #ifndef FILE_H
 	#define FILE_H

 #ifndef DOCUMENT_H
 	#include "111044029HW08Document.h"
 #endif

namespace Tektas{
	class File : public Document {
		public:
			File(string content = "",
				 string pathName = "") :
			     Document(content),
			     _pathName(pathName)
			     { /* Deliberately left blank.*/ };

			//getters
			string getPathName() const;
			string getContent() const;

			//setters
			void setPathName(string pathName);
			void setContent(string content);

			File& operator=(const File& other);
		private:
			string _pathName;
	};
}

#endif // FILE_H