/*
 * File:    111044029HW08Email.h
 *
 * Course:  CSE241
 * Project: HW08
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 10, 2012, 06:29 PM
 */
 
 #ifndef EMAIL_H
 	#define EMAIL_H

 #ifndef DOCUMENT_H
 	#include "111044029HW08Document.h"
 #endif

namespace Tektas{
	class Email : public Document {
		public:
			Email(string title = "",
				  string sender = "",
				  string recipient = "",
				  string body = "") :
			      Document(body),
			      _sender(sender),
			      _recipient(recipient),
			      _title(title)
			{ /* Deliberately left blank.*/ };

			//getters
			string getSender() const;
			string getRecipient() const;
			string getTitle() const;
			string getBody() const;

			//setters
			void setSender(string sender);
			void setRecipient(string recipient);
			void setTitle(string title);
			void setBody(string body);

			Email& operator=(const Email& other);

		private:
			string _sender;
			string _recipient;
			string _title;
	};
}

#endif // EMAIL_H