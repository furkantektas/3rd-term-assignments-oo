/*
 * File:    111044029HW08.cpp
 *
 * Course:  CSE241
 * Project: HW08
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 10, 2012, 06:29 PM
 */

#ifndef FILE_H
	#include "111044029HW08File.h"
#endif

#ifndef EMAIL_H
	#include "111044029HW08Email.h"
#endif

using namespace Tektas;

#include <iostream>


bool containsKeyword( const Document& docObject, string keyword);


int main(int argc, char const *argv[]) {
	using std::cout;
	using std::endl;
	using std::boolalpha;

	File file1("Nulla consequat massa quis enim.",
		       "/home/johm/Desktop/file1.txt"),
	     file2("Quisque rutrum. Aenean imperdiet."),
	     file3;

	Email email1("Hello World - Mail 1!", "john@doe.com",
		         "jane@doe.com" ,
		         "Lorem ipsum dolor sit amet, consectetuer adipiscing elit."),
	      email2("Subject - email2", "tektasfurkan@gmail.com"),
	      email3;

	// Testing setters for Email class
	email2.setBody("Etiam ultricies nisi vel augue.");
	
	email3.setSender("john@doe.com");
	email3.setRecipient("jane@doe.com");
	email3.setTitle("Testing Mail 3");
	email3.setBody("Aenean commodo ligula eget dolor. Aenean massa.");

	// Testing setters for File class
	file2.setPathName("/home/john/Desktop/file2.txt");
	file3.setPathName("/home/john/Desktop/file3.txt");
	file3.setContent("Duis leo. Sed fringilla mauris sit amet nibh.");

	// Testing containsKeyword() function
	cout << boolalpha
	     << "Email1  - ipsum " << containsKeyword(email1,"ipsum") << endl
	     << "Email2  - John " << containsKeyword(email2,"etiam") << endl
	     << "Email3  - Lorem " << containsKeyword(email3,"Lorem") << endl
	     << "File1  - Negative " << containsKeyword(file1,"Negative") << endl
	     << "File2  - rutrum " << containsKeyword(file2,"rutrum") << endl
	     << "File3  - Duis " << containsKeyword(file3,"duis") << endl
	     ;
	return 0;
}

// Testing function for Document objects and objects 
// derived from Document class
bool containsKeyword( const Document& docObject, string keyword) {
	if (docObject.getText().find(keyword) != string::npos)
		return true;
	return false;
}
