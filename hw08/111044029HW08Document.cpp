/*
 * File:    111044029HW08Document.cpp
 *
 * Course:  CSE241
 * Project: HW08
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 10, 2012, 06:29 PM
 */

#ifndef DOCUMENT_H
#include "111044029HW08Document.h"
#endif // DOCUMENT_H

namespace Tektas{

	string Document::getText() const {
		return _text;
	}
	
	void Document::setText(string text) {
		_text = text;
	}
	
	Document& Document::operator=(const Document& other) {
		_text = other._text;
		return *this;
	}
}
