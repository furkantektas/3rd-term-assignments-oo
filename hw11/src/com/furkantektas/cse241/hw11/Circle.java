/*
 * File:    Circle.java
 *
 * Course:  CSE241
 * Project: HW10
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw11;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Scanner;

/**
 *
 * @author furkan
 */
public class Circle extends Ellipse {

    public Circle() {
        super();
    }

    public Circle(int a, Integer[] x, Integer[] y) {
        super(a, a, x, y);
    }

    public Circle(int a, Integer[] x, Integer[] y, double thickness) {
        super(a, a, x, y, thickness);
    }

    @Override
    public void draw(Graphics g, double ratioX, double ratioY) {
        g.setColor(Color.cyan);
        super.draw(g, ratioX, ratioY);
    }
}
