/*
 * File:    Shape.java
 *
 * Course:  CSE241
 * Project: HW11
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw11;

import java.awt.Graphics;
import java.io.Serializable;

/**
 *
 * @author furkan
 */
public interface Shape extends Serializable{

    public double getArea();

    public double getPerimeter();
    /* getter for initial coordinates */

    public Integer[] getXCoordinates();

    public Integer[] getYCoordinates();
    /* setter for initial coordinates */

    /**
     * Sets the x-coordinates of the shape
     *
     * @param x
     * @throws Error if any of coordinate is smaller than 0
     */
    public void setCoordinatesX(Integer[] x) throws ArrayIndexOutOfBoundsException;

    public String listCoordinatesX();

    /**
     * Sets the x-coordinates of the shape
     *
     * @param y
     * @throws Error if any of coordinate is smaller than 0
     */
    public void setCoordinatesY(Integer[] y)
            throws ArrayIndexOutOfBoundsException;

    public String listCoordinatesY();

    /**
     *
     * @param g
     * @param ratioX - ratio to resize(multiply) coordinates
     * @param ratioY - ratio to resize(multiply) coordinates
     * @throws Error if the shape is out of frame
     */
    public void draw(Graphics g, double ratioX, double ratioY)
            throws ArrayIndexOutOfBoundsException;

    public void setStrokeThickness(double thickness);

    public double getStrokeThickness();
}
