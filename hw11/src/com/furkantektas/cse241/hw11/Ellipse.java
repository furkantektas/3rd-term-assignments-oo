/*
 * File:    Ellipse.java
 *
 * Course:  CSE241
 * Project: HW11
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw11;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Scanner;

/**
 *
 * @author furkan
 */
public class Ellipse implements Shape {

    private int _width = 0;
    private int _height = 0;
    private Integer[] _coordinateX = new Integer[1];
    private Integer[] _coordinateY = new Integer[1];
    private double _strokeThickness;

    public Ellipse() throws ArrayIndexOutOfBoundsException {
        setWidth(0);
        setHeight(0);
        setCoordinatesX(new Integer[]{new Integer(0)});
        setCoordinatesY(new Integer[]{new Integer(0)});
        setStrokeThickness(1.0);
    }

    public Ellipse(int width, int height, Integer[] x, Integer[] y)
            throws ArrayIndexOutOfBoundsException {
        setWidth(width);
        setHeight(height);
        setCoordinatesX(x);
        setCoordinatesY(y);
        setStrokeThickness(1.0);
    }

    public Ellipse(int width, int height, Integer[] x, Integer[] y,
            double thickness)
            throws ArrayIndexOutOfBoundsException {
        setWidth(width);
        setHeight(height);
        setCoordinatesX(x);
        setCoordinatesY(y);
        setStrokeThickness(thickness);
    }

    @Override
    public double getArea() {
        return Math.PI * getWidth() * getHeight();
    }

    @Override
    public double getPerimeter() {
        return Math.PI * (3 * (getWidth() + getHeight())
                - Math.sqrt(
                (3 * getHeight() + getWidth())
                * (3 * getWidth() + getHeight())));
    }

    @Override
    public Integer[] getXCoordinates() {
        return _coordinateX;
    }

    @Override
    public Integer[] getYCoordinates() {
        return _coordinateY;
    }

    @Override
    public void setCoordinatesX(Integer[] x) throws IllegalArgumentException {
        if (x[0] >= 0 && x[0] <= Shapes.panelWidth) {
            _coordinateX[0] = x[0];
        } else {
            System.out.println(x[0] + "-" + Shapes.panelHeight);
            throw new ArrayIndexOutOfBoundsException("Invalid coordinates.");
        }
    }

    @Override
    public String listCoordinatesX() {
        return _coordinateX[0].toString();
    }

    @Override
    public void setCoordinatesY(Integer[] y) throws IllegalArgumentException {
        if (y[0] >= 0 && y[0] <= Shapes.panelHeight) {
            _coordinateY[0] = y[0];
        } else {
            System.out.println(y[0] + "-" + Shapes.panelHeight);
            throw new ArrayIndexOutOfBoundsException("Invalid coordinates.");
        }
    }

    @Override
    public String listCoordinatesY() {
        return _coordinateY[0].toString();
    }

    /**
     *
     * Draws an ellipse according to the instance variables.Ellipse's
     * coordinates and line thicknesses can be resizable.
     *
     * @param g
     * @param ratioX current window width/default window width
     * @param ratioY current window height/default window height
     * @throws ArrayIndexOutOfBoundsException
     */
    @Override
    public void draw(Graphics g, double ratioX, double ratioY)
            throws ArrayIndexOutOfBoundsException {
        if ((getXCoordinates()[0] + getWidth() > Shapes.panelWidth)
                || (getYCoordinates()[0] + getHeight() > Shapes.panelHeight)) {
            throw new ArrayIndexOutOfBoundsException("Object is out of boundary.");
        }
        Graphics2D g2d = (Graphics2D) g;

        g2d.setPaintMode();
        g2d.setStroke((new BasicStroke((float) (_strokeThickness * ratioY * ratioX))));
        g2d.drawOval((int) (getXCoordinates()[0] * ratioX), (int) (getYCoordinates()[0] * ratioY),
                (int) (getWidth() * ratioX), (int) (getHeight() * ratioY));
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return _width;
    }

    /**
     * @param a the width to set
     */
    public void setWidth(int a) {
        this._width = a;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return _height;
    }

    /**
     * @param b the height to set
     */
    public void setHeight(int b) {
        this._height = b;
    }

    @Override
    public void setStrokeThickness(double thickness) {
        _strokeThickness = thickness;
    }

    @Override
    public double getStrokeThickness() {
        return _strokeThickness;
    }

    /**
     * Will be used to initialize coordinates on ShapeForm.class
     * @return the _numOfEdge
     */
    public int getNumOfEdge() {
        return 1;
    }
}
