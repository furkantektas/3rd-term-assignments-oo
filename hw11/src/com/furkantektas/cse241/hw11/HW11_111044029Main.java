/*
 * File:    HW11_111044029Main.java
 *
 * Course:  CSE241
 * Project: HW11
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on January 10, 2013, 8:45 PM
 */
package com.furkantektas.cse241.hw11;

/**
 *
 * @author furkan
 */
public class HW11_111044029Main {
    public static void main(String args[]) {
        final MainWindow appFrame = new MainWindow();

        MainWindow.centerFrame(appFrame);
        appFrame.setVisible(true);
    }
}
