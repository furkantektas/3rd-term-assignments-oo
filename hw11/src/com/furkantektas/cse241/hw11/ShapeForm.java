/*
 * File:    ShapeForm.java
 *
 * Course:  CSE241
 * Project: HW11
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on January 10, 2013, 8:45 PM
 */
package com.furkantektas.cse241.hw11;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author furkan
 */
public class ShapeForm extends JFrame {

    private JLabel _xCoordinatesLabel;
    private JLabel _yCoordinatesLabel;
    private JLabel _thicknessLabel;
    private JLabel _radiusHLabel;
    private JLabel _radiusWLabel;
    private int _numOfEdge;
    private JTextField _xCoordinatesTF;
    private JTextField _yCoordinatesTF;
    private JTextField _thicknessTF;
    private JTextField _radiusWTF;
    private JTextField _radiusHTF;
    private JButton _okButton;
    private JButton _cancelButton;
    private Shape _s;
    private final boolean _isEdit;
    private final String _actionName;
    private JPanel _mainPanel;

    /**
     * Alias for created shapes' constructor
     * @param s
     */
    public ShapeForm(Shape s) {
        this(s, true);
    }

    /**
     *
     * @param s
     * @param isEdit true for created shapes, false for new shapes
     */
    public ShapeForm(Shape s, boolean isEdit) {
        _isEdit = isEdit;
        _actionName = ((_isEdit) ? "Edit" : "Create");
        _numOfEdge = (s instanceof Polygon) ? ((Polygon) s).getNumOfEdge() : 1;

        _s = s;

        initializeFormComponents();
        MainWindow.centerFrame(this);
        setTitle(_actionName + " a " + s.getClass().getSimpleName());
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);

    }

    /**
     * Initializes the form's elements and sets the current values for created
     * shapes.
     */
    private void initializeFormComponents() {
        // Creating the gui objects
        _xCoordinatesTF = new JTextField();
        _xCoordinatesLabel = new JLabel();
        _yCoordinatesLabel = new JLabel();
        _yCoordinatesTF = new JTextField();
        _thicknessTF = new JTextField();
        _thicknessLabel = new JLabel();
        _okButton = new JButton();
        _cancelButton = new JButton();

        // Form's default row count
        int rowCount = 4;

        // Specialize the form according to the shape
        if (_s instanceof Circle) {
            _radiusHTF = new JTextField();
            _radiusHTF.setText(String.valueOf(((Ellipse) _s).getHeight()));

            _radiusHLabel = new JLabel("Radius");
            _radiusHLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
            rowCount += 1;
        } else if (_s instanceof Ellipse) {
            _radiusHTF = new JTextField();
            _radiusHTF.setText(String.valueOf(((Ellipse) _s).getHeight()));

            _radiusWTF = new JTextField();
            _radiusWTF.setText(String.valueOf(((Ellipse) _s).getWidth()));

            _radiusHLabel = new JLabel("Height");
            _radiusHLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

            _radiusWLabel = new JLabel("Width");
            _radiusWLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
            rowCount += 2;
        }

        // X coordinates
        _xCoordinatesTF.setText(implodeCoordinates(",", _s.getXCoordinates()));
        _xCoordinatesTF.setToolTipText("0,10,20");

        _xCoordinatesLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        if (_s instanceof Ellipse) {
            _xCoordinatesLabel.setText("Leftmost Coordinate");
        } else {
            _xCoordinatesLabel.setText("X Coordinates(use comma to seperate)");
        }

        // Y coordinates
        _yCoordinatesLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        if (_s instanceof Ellipse) {
            _yCoordinatesLabel.setText("Topmost Coordinate");
        } else {
            _yCoordinatesLabel.setText("Y Coordinates(use comma to seperate)");
        }
        _yCoordinatesTF.setText(implodeCoordinates(",", _s.getYCoordinates()));
        _yCoordinatesTF.setToolTipText("0,50,50");


        // Line thickness
        _thicknessLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        _thicknessLabel.setText("Line Thickness");

        _thicknessTF.setText(String.valueOf(_s.getStrokeThickness()));
        _thicknessTF.setToolTipText("Default: 1.0");


        // Form buttons
        String okButtonLabel = (_isEdit) ? "Update" : "Add";
        String cancelButtonLabel = "Discard";
        _okButton.setText(okButtonLabel);
        _cancelButton.setText(cancelButtonLabel);

        //Button's listeners
        _okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    updateShape();
                    dispose();
                } catch (Throwable exp) {
                    System.err.println(exp.getMessage());
                }
            }
        });

        _cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        // Creating a bordered JPanel to hold form's elements
        _mainPanel = new JPanel(new GridLayout(rowCount, 2, 10, 10));
        _mainPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(10, 10, 10, 10), // outer border
                BorderFactory.createEmptyBorder(10, 10, 10, 10) // inner border
                ));

        // Adding JPanel to frame
        getContentPane().add(_mainPanel);

        // Adding the necessary elements to the form according to shape
        _mainPanel.add(_xCoordinatesLabel);
        _mainPanel.add(_xCoordinatesTF);
        _mainPanel.add(_yCoordinatesLabel);
        _mainPanel.add(_yCoordinatesTF);
        if (_s instanceof Circle) {
            _mainPanel.add(_radiusHLabel);
            _mainPanel.add(_radiusHTF);
        } else if (_s instanceof Ellipse) {
            _mainPanel.add(_radiusHLabel);
            _mainPanel.add(_radiusHTF);
            _mainPanel.add(_radiusWLabel);
            _mainPanel.add(_radiusWTF);
        }
        _mainPanel.add(_thicknessLabel);
        _mainPanel.add(_thicknessTF);
        _mainPanel.add(_cancelButton);
        _mainPanel.add(_okButton);

        pack();
    }

    /**
     * Updates shape information after the confirmation button was clicked
     * @throws Throwable
     */
    private void updateShape() throws Throwable {
        Integer[] x = null;
        Integer[] y = null;
        double thickness = 0.0;
        int radiusH = 0, radiusW = 0;
        try {
            /* Getting X Coordinates for created shape */
            x = parseCoordinates(_xCoordinatesTF.getText());
            _s.setCoordinatesX(x);

            /* Getting Y Coordinates for created shape */
            y = parseCoordinates(_yCoordinatesTF.getText());
            _s.setCoordinatesY(y);

            // Specializing the form's labels and error messages for ellipse
            // and circle
            if(_s instanceof Circle) {
                radiusH = Integer.valueOf(_radiusHTF.getText());
                if (!validateRadius(radiusH)) {
                    throw new Exception("Invalid Radius");
                }
                ((Ellipse)_s).setHeight(radiusH);
                ((Ellipse)_s).setWidth(radiusH);
            }else if (_s instanceof Ellipse) {
                radiusH = Integer.valueOf(_radiusHTF.getText());
                if (!validateRadius(radiusH)) {
                    throw new Exception("Invalid Height");
                }
                ((Ellipse)_s).setHeight(radiusH);

                if (!validateRadius(radiusH)) {
                    throw new Exception("Invalid Height");
                }

                radiusW = Integer.valueOf(_radiusWTF.getText());

                ((Ellipse)_s).setWidth(radiusW);

                if (!validateRadius(radiusW)) {
                    throw new Exception("Invalid Width");
                }
            }

            // Line thickness
            thickness = Double.valueOf(_thicknessTF.getText());
            if (!validateStroke(thickness)) {
                throw new Exception("Invalid stroke");
            }
            _s.setStrokeThickness(thickness);

        } catch (Throwable e) {
            // Prompting on the stderr for exceptions and rethrowing them
            System.err.println(e.getMessage());
            throw e;
        } finally {
            // Validity checking before the form's elements being processed

            // If validity checking fails, user will be warned by dialog
            // and the failed inputbox's border will turn to red

            if (!validateCoordinates(x)) {
                warnTextField(_xCoordinatesTF);
                showErrorDialog("X Coordinates");
                throw new Exception("Invalid x coordinates");
            }
            if (!validateCoordinates(y)) {
                warnTextField(_yCoordinatesTF);
                showErrorDialog("Y Coordinates");
                throw new Exception("Invalid y coordinates");
            }
            if(_s instanceof Circle) {
                if(!validateRadius(radiusH)) {
                   warnTextField(_radiusHTF);
                    showErrorDialog("Radius");
                    throw new Exception("Invalid Radius");
                }
            }else if (_s instanceof Ellipse) {
                if(!validateRadius(radiusH)) {
                   warnTextField(_radiusHTF);
                    showErrorDialog("Height");
                    throw new Exception("Invalid Height");
                }
                if(!validateRadius(radiusW)) {
                   warnTextField(_radiusWTF);
                    showErrorDialog("Width");
                    throw new Exception("Invalid Width");
                }
            }
            if (!validateStroke(thickness)) {
                warnTextField(_thicknessTF);
                showErrorDialog("Line Thickness");
                throw new Exception("Invalid thickness");
            }
            _mainPanel.repaint();
        }
    }

    /**
     * Shows the appropriate error dialog for failed validity check
     * @param message failed label name
     */
    private void showErrorDialog(String message) {
        JOptionPane.showMessageDialog(null, "Invalid "+message+"!\nMouseover the input field to see hints", "Error: "+message, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Turns the text field's border to red.
     * @param tf JTextField to be effected
     */
    private void warnTextField(JTextField tf) {
        tf.setBorder(BorderFactory.createLineBorder(Color.RED));
    }

    /**
     * Coordinate parser from string
     * @param coordStr text field's value
     * @return coordinate array (Integer[])
     * @throws NumberFormatException for empty or invalid coordStr
     */
    private Integer[] parseCoordinates(String coordStr) throws NumberFormatException {
        if (coordStr.length() == 0) {
            throw new NumberFormatException();
        }

        String[] xCoords = coordStr.split(",");
        final int xCoordLength = xCoords.length;
        Integer[] coordArr = new Integer[xCoordLength];
        for (int i = 0; i < xCoordLength; ++i) {
            coordArr[i] = Integer.parseInt(xCoords[i]);
        }
        return coordArr;
    }

    /**
     * Checks the validity of the given coordinates
     * @param coords
     * @return
     */
    private boolean validateCoordinates(Integer[] coords) {
        boolean valid = true;
        if ((coords == null) || (coords.length == 0) || (coords.length != _numOfEdge)) {
            return false;
        }
        for (Integer c : coords) {
            valid = valid && validateCoordinate(c);
        }
        return valid;
    }

    /**
     * Checks the validity for stroke thickness
     * @param thickness
     * @return
     */
    private boolean validateStroke(double thickness) {
        return (thickness > 0.0);
    }

    /**
     * Checks the validity for one coordinate.Used for ellipse, circle and
     * validateCoordinates()
     * @param coordinate
     * @return
     */
    private boolean validateCoordinate(Integer coordinate) {
        return (coordinate >= 0);
    }

    /**
     * Checks the validity for ellipse's and circle's radius
     * @param radius
     * @return
     */
    private boolean validateRadius(int radius) {
        return (radius > 0);
    }

    /**
     * Implodes the string.Used in parsing coordinates.
     * @param separator comma for coordinates
     * @param data text fields' value
     * @return
     */
    public static String implodeCoordinates(String separator, Integer... data) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length - 1; i++) {
            if (!data[i].toString().matches(" *")) {//empty string are ""; " "; "  "; and so on
                sb.append(data[i]);
                sb.append(separator);
            }
        }
        sb.append(data[data.length - 1]);
        return sb.toString();
    }
}