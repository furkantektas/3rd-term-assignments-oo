/*
 * File:    MainWindow.java
 *
 * Course:  CSE241
 * Project: HW11
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on January 10, 2013, 8:45 PM
 */
package com.furkantektas.cse241.hw11;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DecimalFormat;
import java.util.Random;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import sun.awt.X11.Screen;

/**
 *
 * @author furkan
 */
public class MainWindow extends JFrame {

    public static final int FRAMEWIDTH = Shapes.panelWidth;
    public static final int FRAMEHEIGHT = Shapes.panelHeight;
    public static final String[] ShapeNames = {"Ellipse", "Circle", "Triangle",
        "Rectangle", "Hexagon"};

    Shapes shapeList = new Shapes();
    JButton[] buttons = new JButton[4];
    JPanel footerMenu = new JPanel(new GridLayout(1, 5));
//    Choice shapeChooser = new Choice();
    JComboBox shapeChooser = new JComboBox();

    private JMenuBar _menuBar = new JMenuBar();
    private JMenu _menu = new JMenu("Options");

    Icon[] icons = new ImageIcon[]{
            new ImageIcon(getClass().getResource("edit.png")),
            new ImageIcon(getClass().getResource("info.png")),
            new ImageIcon(getClass().getResource("delete.png")),
            new ImageIcon(getClass().getResource("new.png"))
    };

    public MainWindow() {
        super();
        this.setTitle("111044029 - HW11");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setIconImage(new ImageIcon(getClass().getResource("app.png")).getImage());
        this.setSize(new Dimension(672,480));
        this.setResizable(false);

        // Creating the JMenu and setting keyboard shortcuts and mnemonics
        JMenuItem importItem = new JMenuItem("Import");
        JMenuItem exportItem = new JMenuItem("Export");

        _menu.setMnemonic(KeyEvent.VK_O);
        importItem.setMnemonic(KeyEvent.VK_I);
        exportItem.setMnemonic(KeyEvent.VK_E);

        // Ctrl+I will call the import
        importItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I,
            ActionEvent.CTRL_MASK));

        // Ctrl+E will call the export
        exportItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,
            ActionEvent.CTRL_MASK));

        // Setting import menu item's listeners
        importItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                shapeList.importShapes();
                repaint();
            }
        });

        // Setting export menu item's listeners
        exportItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                shapeList.exportShapes();
            }
        });

        // Setting the icons for menu items to make usage easier
        _menu.setIcon(new ImageIcon(getClass().getResource("options.png")));
        importItem.setIcon(new ImageIcon(getClass().getResource("import.png")));
        exportItem.setIcon(new ImageIcon(getClass().getResource("export.png")));

        // Adding the menu items to the Options menu
        _menu.add(importItem);
        _menu.add(exportItem);

        // Adding options menu to menubar and setting _menuBar to be the default
        // menu bar for the Mainwindow frame.
        _menuBar.add(_menu);
        this.setJMenuBar(_menuBar);

        // Setting shape panel's background to white
        shapeList.setBackground(Color.WHITE);

        // Adiing the footer menu to the bottom
        footerMenu.setSize(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, 30);
        this.add(footerMenu, BorderLayout.SOUTH);


        // Adding footerMenu's components
        footerMenu.add(shapeChooser);

        buttons[0] = new JButton("Edit");
        buttons[1] = new JButton("Info");
        buttons[2] = new JButton("Delete");
        buttons[3] = new JButton("New Shape");

        // Setting icons for footerMenu's buttons
        buttons[0].setIcon(icons[0]);
        buttons[1].setIcon(icons[1]);
        buttons[2].setIcon(icons[2]);
        buttons[3].setIcon(icons[3]);

        // Edit Button's listener
        buttons[0].addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                manageShapes();
            }
        });

        // Info Button's listener
        buttons[1].addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Shape temp = shapeList.getShape(shapeChooser.getSelectedIndex());
                DecimalFormat df = new DecimalFormat("#.##");
                String shapeInfo = "Shape: "+temp.getClass().getSimpleName()+
                                   "\nPerimeter = "+df.format(temp.getPerimeter())+
                                   "\nArea = "+df.format(temp.getArea());
                JOptionPane.showMessageDialog(null, shapeInfo, "Shape Info",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });

        // Delete Button's listener
        buttons[2].addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    shapeList.removeShape(shapeChooser.getSelectedIndex());
                    repaint();
                } catch (IndexOutOfBoundsException exp) {
                    JOptionPane.showMessageDialog(null, "No shape found!","Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        // New Shape button's listener
        buttons[3].addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                createShape();
                repaint();
            }
        });
        for(JButton but : buttons) {
            footerMenu.add(but);
        }

        try {

            // Creating some random shapes with random stroke width
            shapeList.addShape(getRandomCircle());
            shapeList.addShape(getRandomEllipse());
            shapeList.addShape(getRandomTriangle());
            shapeList.addShape(getRandomRectangle());
            shapeList.addShape(getRandomHexagon());
            shapeList.addShape(getRandomCircle());
            shapeList.addShape(getRandomEllipse());

            repaint();
        } catch (Exception exp) {
            JOptionPane.showMessageDialog(this, exp.getMessage(),"Error",
                    JOptionPane.ERROR_MESSAGE);
            System.err.println(exp.getMessage());
        } finally {
            // adding the shape panel to the top
            this.add(shapeList,BorderLayout.NORTH);
            this.pack();
        }

    }

    /**
     * Generates a random Ellipse
     * @return Ellipse
     */
    public static Ellipse getRandomEllipse() {
        return new Ellipse(
            getRandomInt(FRAMEHEIGHT - 300),
            getRandomInt(FRAMEHEIGHT - 300),
            getRandomCoordinates(FRAMEWIDTH - 300,1),
            getRandomCoordinates(FRAMEHEIGHT - 300,1),
            (getRandomInt(20) / 5.0));
    }

    /**
     * Generates a random Circle
     * @return Circle
     */
    public static Circle getRandomCircle() {
        return new Circle(
            getRandomInt(FRAMEHEIGHT - 300),
            getRandomCoordinates(FRAMEWIDTH - 300,1),
            getRandomCoordinates(FRAMEHEIGHT - 300,1),
            (getRandomInt(20) / 5.0));
    }

    /**
     * Generates a random Triangle
     * @return Triangle
     */
    public static Triangle getRandomTriangle() {
        return new Triangle(
            getRandomCoordinates(FRAMEWIDTH,3),
            getRandomCoordinates(FRAMEHEIGHT,3),
            (getRandomInt(20) / 5.0));
    }

    /**
     * Generates a random Rectangle
     * @return Rectangle
     */
    public static Rectangle getRandomRectangle() {
        return new Rectangle(
            getRandomCoordinates(FRAMEWIDTH,4),
            getRandomCoordinates(FRAMEHEIGHT,4),
            (getRandomInt(20) / 5.0));
    }

    /**
     * Generates a random Hexagon
     * @return Hexagon
     */
    public static Hexagon getRandomHexagon() {
        return new Hexagon(
            getRandomCoordinates(FRAMEWIDTH,6),
            getRandomCoordinates(FRAMEHEIGHT,6),
            (getRandomInt(20) / 5.0));
    }

    /**
     * Generates a random coordinate array for creating random shapes
     * @return Hexagon
     */
    public static Integer[] getRandomCoordinates(int max, int size) {
        Integer[] arr = new Integer[size];
        for(int i=0; i<size;++i) {
            arr[i] = new Integer(getRandomInt(max));
        }
        return arr;
    }

    /**
     * Generates a random int for creating random   shapes
     * @return Hexagon
     */
    public static int getRandomInt(int max) {
        Random r = new Random();
        return Math.abs(r.nextInt(max));

    }

    /**
     * Centers the frame on the screen.
     * @param frame JFrame
     */
    public static void centerFrame(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }

    /**
     * Called after the New Shape button was clicked.Create a new shape
     * according to the user's choice.Uses ShapeForm.java to set shape's
     * properties.
     * @param shapeList
     */
    public void createShape() {
        int userChoice = JOptionPane.showOptionDialog(null, "Choose a shape",
                "New Shape", JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE,
                null, ShapeNames, 0);
        Shape tempShape;

        switch (userChoice) {
            case 0:
                tempShape = new Ellipse();
                break;
            case 1:
                tempShape = new Circle();
                break;
            case 2:
                tempShape = new Triangle();
                break;
            case 3:
                tempShape = new Rectangle();
                break;
            case 4:
                tempShape = new Hexagon();
                break;
            default:
                tempShape = new Rectangle();
        }
        try {
            ShapeForm pf = new ShapeForm(tempShape,false);
            shapeList.addShape(tempShape);
            pf.addWindowListener(new WindowListener() {

                @Override
                public void windowOpened(WindowEvent e) {
                }

                @Override
                public void windowClosing(WindowEvent e) {
                }

                @Override
                public void windowClosed(WindowEvent e) {
                    repaint();
                }

                @Override
                public void windowIconified(WindowEvent e) {
                }

                @Override
                public void windowDeiconified(WindowEvent e) {
                }

                @Override
                public void windowActivated(WindowEvent e) {
                }

                @Override
                public void windowDeactivated(WindowEvent e) {
                }
            });
        } catch (IndexOutOfBoundsException e) {
            System.err.println(e.getMessage());
        }
    }

    public void fillShapeChooser(JComboBox chooser) {
        chooser.removeAllItems();
        for (int i=0;i<shapeList.getShapeNum();++i) {
            chooser.addItem((i+1) + ") " + shapeList.getShape(i).getClass().getSimpleName());
        }
    }

    public void manageShapes() {
        Shape s = shapeList.getShape(shapeChooser.getSelectedIndex());
        ShapeForm pf = new ShapeForm(s);
        pf.addWindowListener(new WindowListener() {

            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {

            }

            @Override
            public void windowClosed(WindowEvent e) {
                repaint();
            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
    }

    @Override
    public void repaint() {
        super.repaint();
        fillShapeChooser(shapeChooser);
    }

}