/*
 * File:    Shapes.java
 *
 * Course:  CSE241
 * Project: HW11
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw11;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author furkan
 */
public class Shapes extends JPanel {

//    private Shape[] _shapeList;
    private ArrayList<Shape> _shapeList = new ArrayList<Shape>();
    public static final int panelWidth = 650;
    public static final int panelHeight = 400;

    private String _fileName = "data.bin";

    public Shapes() {
        this.setPreferredSize(new Dimension(panelWidth, panelHeight));
    }

    public void addShape(Shape newShape) {
        _shapeList.add(newShape);
    }

    public void removeShape(int index) throws IndexOutOfBoundsException {
        _shapeList.remove(index);
    }

    public Shape getShape(int index) throws IndexOutOfBoundsException {
        return _shapeList.get(index);
    }

    @Override
    public void paintComponent(Graphics g) {

        double ratioX = (double) getWidth() / panelWidth;
        double ratioY = (double) getHeight() / panelHeight;

        super.paintComponent(g);
        shapeLoop:
        for (Shape shape : _shapeList) {
            try {
                shape.draw(g, ratioX, ratioY);
            } catch (Exception e) {
                _shapeList.remove(shape); // removing the invalid shape
                System.err.println("Invalid Object: "
                        + shape.getClass().getSimpleName()
                        + "\nShape was removed.");
                JOptionPane.showMessageDialog(null, "Invalid shape coordinates.\n"+
                        "Shape was removed and the future shapes will be discarded.",
                        "Error: Invalid shape", JOptionPane.ERROR_MESSAGE);
                break shapeLoop;
            }
        }
    }

    public int getShapeNum() {
        return _shapeList.size();
    }

    /**
     * Imports shapes from _fileName binary file.
     */
    public void importShapes() {
        try{
            ObjectInputStream inputStream =
                    new ObjectInputStream(
                            new FileInputStream (_fileName));
            _shapeList = (ArrayList<Shape>)inputStream.readObject();
            inputStream.close();
            repaint();
            JOptionPane.showMessageDialog(null, "Shapes are imported from file: "
                    +_fileName, "Shapes are Imported", JOptionPane.INFORMATION_MESSAGE);
        }
        catch (IOException | ClassNotFoundException | HeadlessException e){
            System.err.println("Problem reading the file " + _fileName);
            JOptionPane.showMessageDialog(null, "Shapes couldn't imported from file: "
                    +_fileName, "Shapes could not be imported\n"+
                    "Be sure that you exported before importing the shapes.",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Exports shapes to the _fileName binary file.
     */
    public void exportShapes() {
        try{
            ObjectOutputStream outputStream =
                    new ObjectOutputStream(
                            new FileOutputStream (_fileName));
            outputStream.writeObject(_shapeList);
            outputStream.close( );
            JOptionPane.showMessageDialog(null, "Shapes are exported to file: "+_fileName,
                    "Shapes Exported", JOptionPane.INFORMATION_MESSAGE);
        }
        catch ( IOException e ){
            JOptionPane.showMessageDialog(null, "Couldn't write to file", "Error: Export",
                    JOptionPane.ERROR_MESSAGE);
            System.err.println("Error writing to file: "+e);
        }
    }
}
