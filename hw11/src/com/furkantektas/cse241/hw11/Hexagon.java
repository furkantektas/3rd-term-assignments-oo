/*
 * File:    Hexagon.java
 *
 * Course:  CSE241
 * Project: HW10
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw11;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author furkan
 */
public class Hexagon extends Polygon {

    public Hexagon() {
        super(6);
    }

    public Hexagon(Integer[] x, Integer[] y) {
        super(6, x, y);
    }

    public Hexagon(Integer[] x, Integer[] y, double thickness) {
        super(6, x, y, thickness);
    }

    @Override
    public void draw(Graphics g, double ratioX, double ratioY) {
        g.setColor(Color.MAGENTA);
        super.draw(g, ratioX, ratioY);
    }
}
