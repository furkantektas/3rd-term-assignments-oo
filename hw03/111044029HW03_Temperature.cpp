/*
 * File:    111044029HW03_Temperature.cpp
 *
 * Course:  CSE241
 * Project: HW03
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on October 22, 2012, 8:45 PM
 */

#ifndef TEMPERATURE_H
    #include "111044029HW03_Temperature.h"
#endif

using std::cout;
using std::cin;
using std::setprecision;
using std::endl;
using std::setiosflags;
using std::ios;
using std::exit;

void Temperature::setTempKelvin(double kelvinInput) {
    if (validateInput(kelvinInput))
        kelvin = kelvinInput;
    else {
        cout << "Kelvin degrees cannot be less than 0.\nTerminating\n";
        exit(1);
    }

}

void Temperature::setTempCelsius(double celsius) {
    setTempKelvin(celsius2Kelvin(celsius));
}

void Temperature::setTempFahrenheit(double fahrenheit) {
    setTempKelvin(fahrenheit2Kelvin(fahrenheit));
}

double Temperature::getTempKelvin() const {
    return kelvin;
}

double Temperature::getTempCelsius() const {
    return kelvin2Celsius(getTempKelvin());
}

double Temperature::getTempFahrenheit() const {
    return kelvin2Fahrenheit(getTempKelvin());
}

void Temperature::output() const {
    cout << setiosflags(ios::fixed) << setprecision(2)
            << "Kelvin     : " << getTempKelvin() << endl
            << "Celsius    : " << getTempCelsius() << endl
            << "Fahrenheit : " << getTempFahrenheit() << endl;
}

double Temperature::kelvin2Fahrenheit(double kelvin) const {
    return kelvin2Celsius(kelvin)*(9.0 / 5.0) + 32;
}

inline double Temperature::kelvin2Celsius(double kelvin) const {
    return kelvin - 273.15;
}

double Temperature::fahrenheit2Kelvin(double fahrenheit) const {
    return celsius2Kelvin((5.0 / 9.0)*(fahrenheit - 32));
}

inline double Temperature::celsius2Kelvin(double celsius) const {
    return celsius + 273.15;
}

inline bool Temperature::validateInput(double kelvin) const {
    return (kelvin > 0);
}