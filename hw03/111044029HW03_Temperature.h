/*
 * File:    111044029HW03_Temperature.h
 *
 * Course:  CSE241
 * Project: HW03
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on October 22, 2012, 8:41 PM
 */

#ifndef TEMPERATURE_H
#define	TEMPERATURE_H

#include <iostream> // for cin and cout
#include <iomanip>  // for output formatting
#include <cstdlib>  // for exit
#include <cctype>   // for atoi

class Temperature {
    public:
        void setTempKelvin(double kelvinInput);
        void setTempCelsius(double celsius);
        void setTempFahrenheit(double fahrenheit);

        double getTempKelvin() const;
        double getTempCelsius() const;
        double getTempFahrenheit() const;

        void output() const;
        // displays temperature in kelvin, celsius, fahrenheit
    private:
        double kelvin;

        // converters
        double kelvin2Fahrenheit(double kelvin) const;
        inline double kelvin2Celsius(double kelvin) const;
        double fahrenheit2Kelvin(double fahrenheit) const;
        inline double celsius2Kelvin(double celsius) const;

        // checks whether the kelvin is positive
        inline bool validateInput(double kelvin) const;
};
#endif	/* TEMPERATURE_H */