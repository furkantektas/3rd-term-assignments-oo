/*
 * File:    111044029HW03.cpp
 *
 * Course:  CSE241
 * Project: HW03
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on October 22, 2012, 9:06 PM
 */

#include "111044029HW03_Temperature.h"

using namespace std;

void printUsage(void);
// Prints the usage of the program

/* Driver function for Temperature class. */
int main(int argc, char** argv) {
    char tempScale = '\0';
    double input;

    // correct number of parameter
    if(argc == 3) {
        //assigning parameters to variables
        tempScale = argv[1][0];
        input = atof(argv[2]);

        // printing the assigned values
        cout << "Temperature Scale: " << tempScale
             << " Temperature: " << input << endl;
    }
    // program called with incorrect number of parameters
    else if(argc != 0 && argc != 1) {
        printUsage();
        return 0;
    }
    // no parameters entered, get variable's values from user
    else {
        cout << "Select a temperature scale: \n"
             << "[f] Fahrenheit - [c] Celsius - [k] Kelvin\n";
        cin  >> tempScale;


        cout << "Enter degree : ";
        cin  >> input;
    }
    Temperature temp;

    // setting the temp according to temperature scale
    switch(tempScale) {
        case 'f' : case 'F' :
            temp.setTempFahrenheit(input);
            break;
        case 'c' : case 'C' :
            temp.setTempCelsius(input);
            break;
        case 'k' : case 'K' :
            temp.setTempKelvin(input);
            break;
        default:
            cout << "Invalid temperature scale.\nTerminating.";
            return -1;
    }
    // calling Temperature class' output function to show results
    temp.output();

    return 0;
}

void printUsage(void) {
    cout << "Usage: ProgramName TemperatureScale[f|c|k] Temperature\n";
}