/*
 * File:    111044029HW09ArraySet.cpp
 *
 * Course:  CSE241
 * Project: HW09
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 15, 2012, 8:45 PM
 */
#ifndef ARRAYSET_H
    #include "111044029HW09ArraySet.h"
#endif

namespace Tektas {
    template<typename T>
    ArraySet<T>::ArraySet() : _data(0), _size(0), _capacity(0) {}

    template<typename T>
    ArraySet<T>::ArraySet(int capacity) : _data(0), _size(0), _capacity(capacity){
        resize(10);
    }

    template<typename T>
    ArraySet<T>::ArraySet(const ArraySet<T>& other) : _capacity(0), _size(0) {
        resize(other._size); // resize changes the _capacity
        _size = other._size;
        for(int i=0; i<_size;++i)
            _data[i] = other._data[i];
    }

    template<typename T>
    ArraySet<T>& ArraySet<T>::operator=(const ArraySet<T>& other) {
        resize(other._size); // resize changes the _capacity
        _size = other._size;
        for(int i=0; i<_size;++i)
            _data[i] = other._data[i];
    }

    template<typename T>
    ArraySet<T>::~ArraySet(){ delete [] _data; }

    template<typename T>
    void ArraySet<T>::push_back(const T& element) {
        if(_size >= _capacity)
            increaseCapacity();
            // allocate more memory if there is no enough memory
        _data[_size++] = element;
    }

    template<typename T>
    void ArraySet<T>::pop_back() {
        if(_size > 0)
            --_size;
    }


    template<typename T>
    void ArraySet<T>::resize(int capacity) {
        if(capacity > _size) {
            T* temp = _data;
            _data = new T[capacity];
            for(int i=0; i<_size;++i)
                _data[i] = temp[i];
            if(_capacity > 0)
                delete [] temp;
            _capacity = capacity;
        }
    }

    template<typename T>
    void ArraySet<T>::increaseCapacity() {
        resize(_capacity*1.5 + 10);
    }

    template<typename T>
    void ArraySet<T>::add(const T& element) {
        if(Set<T>::search(element) == end())
            push_back(element);
    }


    template<typename T>
    void ArraySet<T>::remove(const T& element) {
         iterator pos = Set<T>::search(element);
         if(pos != end()) {
            T* temp = _data;
            _data = new T[--_capacity];
            int tempSize = _size;
            _size = 0;
            for(int i=0; i<tempSize;++i)
                if(temp[i] != element)
                    add(temp[i]);
            if(_capacity >= 0)
                delete [] temp;
         }
    }

    template<typename T>
    int ArraySet<T>::cardinality() {
        return _size;
    }

    template<typename T>
    typename ArraySet<T>::iterator ArraySet<T>::begin() {
        return iterator(_data);
    }

    template<typename T>
    typename ArraySet<T>::iterator ArraySet<T>::end() {
        return iterator(_data + _size);
        // note that end is the next memory address
        // of the data
    }

    template<typename T>
    typename ArraySet<T>::const_iterator ArraySet<T>::cbegin() {
        return const_iterator(_data);
    }

    template<typename T>
    typename ArraySet<T>::const_iterator ArraySet<T>::cend() {
        return const_iterator(_data + _size);
        // note that end is the next memory address
        // of the data
    }

    template<typename T>
    ArraySet<T> ArraySet<T>::operator%(ArraySet<T>& other) {
        ArraySet<T> temp;
        for(iterator itr = begin(); itr != end(); ++itr)
            if(other.search(*itr) != other.end())
                temp.add(*itr);
        return temp;
    }

    template<typename T>
    ArraySet<T> ArraySet<T>::operator+(ArraySet<T>& other) {
        ArraySet<T> temp = *this;
        for(iterator itr = other.begin(); itr != other.end(); ++itr)
            temp.add(*itr);
        return temp;
    }


}