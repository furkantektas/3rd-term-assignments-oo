/*
 * File:    111044029HW09.cpp
 *
 * Course:  CSE241
 * Project: HW09
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 15, 2012, 8:45 PM
 */

#include <iostream>

#ifndef VECTORSET_H
    #include "111044029HW09VectorSet.h"
    #include "111044029HW09VectorSet.cpp"
#endif
#ifndef ARRAYSET_H
    #include "111044029HW09ArraySet.h"
    #include "111044029HW09ArraySet.cpp"
#endif

using namespace std;
using namespace Tektas;

int main(int argc, char** argv) {

    cout << "=================VectorSet testing=================\n";
    VectorSet<int> a;
    a.add(1);
    a.add(2);
    a.add(3);
    a.add(4);
    a.add(5);
    a.add(5);
    a.add(5);
    a.add(5);
    a.add(6);
    a.add(7);

    VectorSet<int> b;
    b.add(11);
    b.add(2);
    b.add(13);
    b.add(14);
    b.add(15);
    b.add(5);
    b.add(5);
    b.add(5);
    b.add(26);
    b.add(37);

    cout << "A =    " << a;
    cout << "B =    " << b;

    VectorSet<int> tempVec = a%b;
    cout << "A/B =  " << tempVec;
    tempVec = a+b;
    cout << "AUB =  " << tempVec;

    a+=b;
    cout << "A+=B = " << a;

    if(a.search(5) == a.end())
        cout << "5 not found in A\n";
    else
        cout << "5 found in A\n";

    if(a.search(9) == a.end())
        cout << "9 not found in A\n";
    else
        cout << "9 found in A\n";

    a.remove(4);
    cout << "Removing 4 from A => " << a;

    cout << "=================ArraySet testing=================\n";
    ArraySet<int> c;
    c.add(1);
    c.add(2);
    c.add(3);
    c.add(4);
    c.add(5);
    c.add(5);
    c.add(5);
    c.add(5);
    c.add(6);
    c.add(7);

    ArraySet<int> d;
    d.add(11);
    d.add(2);
    d.add(13);
    d.add(14);
    d.add(15);
    d.add(5);
    d.add(5);
    d.add(5);
    d.add(26);
    d.add(37);

    cout << "C =    " << c;
    cout << "D =    " << d;

    ArraySet<int> tempArray = c%d;
    cout << "C/D =  " << tempArray;
    tempArray = c+d;
    cout << "CUD =  " << tempArray;

    c+=d;
    cout << "C+=D = " << c;

    if(c.search(5) == c.end())
        cout << "5 not found in C\n";
    else
        cout << "5 found in C\n";

    if(c.search(9) == c.end())
        cout << "9 not found in C\n";
    else
        cout << "9 found in C\n";

    c.remove(4);
    cout << "Removing 4 from C => " << c;

    return 0;
}