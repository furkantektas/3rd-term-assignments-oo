/*
 * File:    111044029HW09ArraySet.h
 *
 * Course:  CSE241
 * Project: HW09
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 15, 2012, 8:45 PM
 */
#ifndef ARRAYSET_H
#define	ARRAYSET_H

#ifndef SETH
    #include "111044029HW09Set.h"
    #include "111044029HW09Set.cpp"
#endif

namespace Tektas {
    template <typename T>
    class ArraySet : public Set<T> {

        public:

            typedef typename Set<T>::iterator iterator;
            typedef typename Set<T>::const_iterator const_iterator;

            ArraySet();
            ArraySet(int capacity);

            ArraySet(const ArraySet<T>& other);

            ArraySet<T>& operator=(const ArraySet<T>& other);

            virtual ~ArraySet();

            void push_back(const T& element);
            void pop_back();


            void resize(int capacity);
            void increaseCapacity();

            virtual void add(const T& element);
            virtual void remove(const T& element);

            virtual int cardinality();
            virtual iterator begin();
            virtual iterator end();
            virtual const_iterator cbegin();
            virtual const_iterator cend();

        /**
         * Returns the intersection of this set and other set.
         * @param other ArraySet<T>&
         * @return ArraySet<T>
         */
        ArraySet<T> operator%(ArraySet<T>& other);

        /**
         * Returns the union of this set and other set.
         * @param other ArraySet<T>&
         * @return ArraySet<T>
         */
        ArraySet<T> operator+(ArraySet<T>& other);

        private:
            T* _data;
            unsigned int _size;
            unsigned int _capacity;

    }; // class ArraySet
} // namespace Tektas

#endif	/* ARRAYSET_H */

