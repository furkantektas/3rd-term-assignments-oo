/*
 * File:    111044029HW09Set.h
 *
 * Course:  CSE241
 * Project: HW09
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 15, 2012, 8:45 PM
 *
 * For creating STL-compatible iterators :
 * http://stackoverflow.com/a/7759622
 */

#ifndef SETH
#define SETH

//#ifndef ITERATOR_H
//#include "111044029HW09Iterator.h"
//#endif

#include <ostream>// for ostream
#include <iterator> // for std compatible iterator

namespace Tektas {

    template <typename T>
    class Set {
    public:

        // special thanks to Iakov Gnusin for this excellent blog post:
        // http://zotu.blogspot.com/2010/01/creating-random-access-iterator.html
        template <typename P = T>
        class BasicSequantialIterator :
                std::iterator<std::bidirectional_iterator_tag, T> {
            public:
                typedef
                    std::bidirectional_iterator_tag
                    iterator_category;
                typedef typename
                    std::iterator<std::bidirectional_iterator_tag,T>::value_type
                    value_type;
                typedef typename
                   std::iterator<std::bidirectional_iterator_tag, T>::difference_type
                   difference_type;
                typedef typename
                   std::iterator<std::bidirectional_iterator_tag, T>::reference
                   reference;
                typedef typename
                    std::iterator<std::bidirectional_iterator_tag, T>::pointer
                    pointer;
                typedef BasicSequantialIterator<P> selfType;
                typedef BasicSequantialIterator<const P> constType;

            public:
                 BasicSequantialIterator():
                    _ptr(0)
                    {/*deliberately left blank*/}

                 BasicSequantialIterator(pointer ptr):
                     _ptr(ptr)
                     {/*deliberately left blank*/}

                template<typename R>
                BasicSequantialIterator(const BasicSequantialIterator<R>& r):
                    _ptr(dynamic_cast<R*>(&(*r)))
                    {/*deliberately left blank*/}

                template<typename R>
                selfType& operator=(const BasicSequantialIterator<R>& r)
                    { _ptr = &(*r); return *this; }

                P& operator*() const
                    { return *_ptr; }
                bool operator==(const selfType& other) const
                    { return (_ptr == other._ptr); }
                bool operator!=(const selfType& other) const
                    { return !(*this == other); }
                selfType& operator++()
                    {++_ptr; return *this;}
                selfType operator++(int)
                    {return selfType(_ptr++);}
                selfType& operator--()
                    {--_ptr; return *this;}
                selfType operator--(int)
                    {return selfType(_ptr--);}
                selfType& operator+=(size_t value)
                    {_ptr += value; return *this;}
                selfType& operator-=(size_t value)
                    {_ptr -= value; return *this;}
             private:
                P* _ptr;
        };

        typedef BasicSequantialIterator<T> iterator;
        typedef BasicSequantialIterator<const T> const_iterator;

        /**
         * Adds element to the container if container does not contain element
         * @param element
         */
        virtual void add(const T& element) = 0;

        /**
         * Removes element if there is an element in the container
         * @param element
         */
        virtual void remove(const T& element) = 0;

        /**
         * Size of the container
         * @return cardinality of the set
         */
        virtual int cardinality() = 0;

        /**
         *
         * @return iterator of the container's first element
         */
        virtual iterator begin() = 0;

        /**
         *
         * @return iterator to the element one after the actual
         *         end of the container
         */
        virtual iterator end() = 0;

        /**
         *
         * @return const_iterator of the container's first element
         */
        virtual const_iterator cbegin() = 0;
        /**
         *
         * @return const_iterator to the element one after the actual
         *         end of the container
         */
        virtual const_iterator cend() = 0;

        /**
         * Combines two sets in this Set.
         * @param other Set&
         */
        virtual void operator+=(Set<T>& other);

        /**
         *
         * @param element - Element to search in container
         * @return iterator that points to element if element is found.
         *         Unless *this->end() will be returned
         */
        Set::iterator search(const T& element);

        template<typename R>
        friend std::ostream& operator<<(std::ostream& stream, Set<R>& obj);
    };

} // namespace Tektas

#endif  /* SETH */