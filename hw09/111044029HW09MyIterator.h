/*
 * File:   111044029HW09Iterator.h
 * Author: furkan
 *
 * Created on December 16, 2012, 8:21 PM
 */

#ifndef MYITERATOR_H
#define	MYITERATOR_H

namespace Tektas {

    template<typename T>
    class MyIterator {
        public:
            MyIterator() {
                _ptr = new T;
                _ptr = 0; // NULL
            }

            virtual ~MyIterator() {
                delete _ptr;
            }

//            virtual MyIterator operator--(int flag) {
//
//            }
//
//            virtual MyIterator operator--() {
//
//            }
//
//            T& operator*() {
//
//            }
//
//            virtual MyIterator operator++() {
//
//            }
//
//            virtual MyIterator operator++(int flag) {
//
//            }

        private:
                T* _ptr;
    };
}//namespace Tektas


#endif	/* MYITERATOR_H */

