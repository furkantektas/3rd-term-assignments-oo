/*
 * File:    111044029HW09VectorSet.cpp
 *
 * Course:  CSE241
 * Project: HW09
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 15, 2012, 8:45 PM
 */
#ifndef VECTORSET_H
    #include "111044029HW09VectorSet.h"
#endif

namespace Tektas {
    template<typename T>
    void VectorSet<T>::add(const T& element) {
        if(Set<T>::search(element) == end())
            _data.push_back(element);
    }

    template<typename T>
    void VectorSet<T>::remove(const T& element) {
        iterator pos = Set<T>::search(element);
        if(pos != end()) {
            std::remove(_data.begin(),_data.end(),element);
            _data.pop_back(); // resizing the vector
        }
    }

    template<typename T>
    int VectorSet<T>::cardinality() {
        return _data.size();
    }

    template<typename T>
    typename VectorSet<T>::iterator VectorSet<T>::begin() {
        return iterator(&_data[0]);
    }

    template<typename T>
    typename VectorSet<T>::iterator VectorSet<T>::end() {
        return iterator(&_data[_data.size()]);
        // note that end is the next memory address
        // of the data
    }

    template<typename T>
    typename VectorSet<T>::const_iterator VectorSet<T>::cbegin() {
        return const_iterator(&_data[0]);
    }

    template<typename T>
    typename VectorSet<T>::const_iterator VectorSet<T>::cend() {
        return const_iterator(&_data[_data.size()]);
        // note that end is the next memory address
        // of the data
    }

    template<typename T>
    VectorSet<T> VectorSet<T>::operator%(VectorSet<T>& other) {
        VectorSet<T> temp;
        for(iterator itr = begin(); itr != end(); ++itr)
            if(other.search(*itr) != other.end())
                temp.add(*itr);
        return temp;
    }

    template<typename T>
    VectorSet<T> VectorSet<T>::operator+(VectorSet<T>& other) {
        VectorSet<T> temp = *this;
        for(iterator itr = other.begin(); itr != other.end(); ++itr)
            temp.add(*itr);
        return temp;
    }
}
