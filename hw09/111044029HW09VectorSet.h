/*
 * File:    111044029HW09VectorSet.h
 *
 * Course:  CSE241
 * Project: HW09
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 15, 2012, 8:45 PM
 */

#ifndef VECTORSET_H
#define VECTORSET_H


#include <algorithm> // for deleting
#include <vector>
using std::vector;

#ifndef SETH
    #include "111044029HW09Set.h"
    #include "111044029HW09Set.cpp"
#endif

namespace Tektas {

    template <typename T>
    class VectorSet : public Set<T> {

        public:
            typedef typename Set<T>::iterator iterator;
            typedef typename Set<T>::const_iterator const_iterator;

            virtual void add(const T& element);
            virtual void remove(const T& element);
            virtual int cardinality();

            virtual iterator begin();
            virtual iterator end();
            virtual const_iterator cbegin();
            virtual const_iterator cend();

        /**
         * Returns the intersection of this set and other set.
         * @param other VectorSet<T>&
         * @return VectorSet<T>
         */
        VectorSet<T> operator%(VectorSet<T>& other);

        /**
         * Returns the union of this set and other set.
         * @param other VectorSet<T>&
         * @return VectorSet<T>
         */
        VectorSet<T> operator+(VectorSet<T>& other);

        private:
            vector<T> _data;

    }; // class VectorSet

} //namespace Tektas


#endif  /* VECTORSET_H */
