/*
 * File:    111044029HW09Set.cpp
 *
 * Course:  CSE241
 * Project: HW09
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 15, 2012, 8:45 PM
 */
#ifndef SETH
    #include "111044029HW09Set.h"
#endif
namespace Tektas {
    template<typename T>
            void Set<T>::operator+=(Set<T>& other) {
        for (const_iterator itr = other.cbegin(); itr != other.cend(); ++itr)
            if (search(*itr) != other.end())
                this->add(*itr);
    }

    template<typename T>
    typename Set<T>::iterator Set<T>::search(const T& element) {
        typename Set<T>::iterator itr = this->begin();
        for (; itr != this->end(); ++itr)
            if (*itr == element)
                return itr;
        return this->end();
    }

    template<typename T>
    std::ostream& operator<<(std::ostream& stream, Set<T>& obj) {
        stream << "{ ";
        for (typename Set<T>::const_iterator itr = obj.cbegin(); itr != obj.cend(); ++itr)
            stream << *itr << " ";
        return stream << "}\n";
    }
}