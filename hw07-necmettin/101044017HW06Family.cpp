/*######################################################################*/
/*																		*/
/* 101044017HW06Family.cpp												*/
/*																		*/
/*----------------------------------------------------------------------*/
/*																		*/
/* Created by Necmeddin Said Karakoc on 25.11.2012						*/
/*																		*/
/*----------------------------------------------------------------------*/
/*																		*/
/* This source file includes definition of Family class.				*/
/*																		*/
/*######################################################################*/

/*######################################################################*/
/*								Includes								*/
/*######################################################################*/
#include "101044017HW06Family.h"

/*######################################################################*/
/*						Namespace of Karakoc							*/
/*######################################################################*/
namespace Karakoc
{
	/* Initializing static variable */
	int Family::numOfFamilies = 0;
	
	/* Default constructor */
	Family::Family( ) : numOfChildren(0)
	{
		children = new Person[0];
		++numOfFamilies;
	}
	
	/* Constructor that takes two people */
	Family::Family( const Person& mother, const Person& father ) : numOfChildren(0)
	{
		if(mother.getSex() != 'F' || father.getSex() != 'M')
		{
			cout << "Error. Genders are not compatible. " << endl
				 << "Mother must be female and father must be male." << endl;
			
			/* Take a memory to avoid error message on constructor */
			children = new Person[0];	
		}
		
		else
		{
			children = new Person[0];
			this->mother = mother;
			this->father = father;
			++numOfFamilies;
		}
	}
	
	/* Copy constructor */
	Family::Family( const Family& other )
	{
		mother = other.mother;
		father = other.father;
		numOfChildren = other.numOfChildren;
		children = new Person [numOfChildren];
		
		for(int i = 0; i < numOfChildren; ++i)
			children[i] = other.children[i];
	}
	
	/* Overloading assignment operator */
	const Family& Family::operator = ( const Family& other )
	{
		if( this != &other )
		{
			mother = other.mother;
			father = other.father;
			numOfChildren = other.numOfChildren;
			
			delete [] children;
			
			children = new Person [numOfChildren];
			
			for(int i = 0; i < numOfChildren; ++i)
				children[i] = other.children[i];
		}
		
		return *this;
	}
	
	/* Returns child at the given index */
	const Person& Family::getChild( int index ) const
	{
		if(index >= numOfChildren || index < 0)
		{
			cout << "Illegal index for child! Returning with first child." << endl;
			return children[0];
		}
		return children[index];
	}
	
	/* Overloading stream extraction operator */
	//not exactly overloaded.
	istream& operator >> ( istream& is, Family& object )
	{
		
		cout << "For father: ";
		is >> object.father;
		
		cout << "For mother: ";
		is >> object.mother;
		
		int num;
		cout << "Enter number of children >";
		
		is >> num;
		is.get();	//for skip \n character
		while(num < 0)
		{
			cout << "Invalid number. Please enter a valid number >";
			cin >> num;
		}
		
		object.numOfChildren = num;
		
		object.children = new Person [num];
		
		for(int i = 0; i < num; ++i)
		{
			cout << "For " << i+1 << ".child: ";
			is >> object.children[i];
		}
		
		return is;
	}
	
	/* Overloading stream insertion operator */
	ostream& operator << ( ostream& os, const Family& object )
	{
		os << "Mother " << endl
		   << "------" << endl
		   << object.mother << endl;
		os << "Father " << endl
		   << "------" << endl
		   << object.father << endl;
				
		os << "Number of children: " << object.numOfChildren << endl << endl;
		
		for(int i = 0; i < object.numOfChildren; ++i)
			os << "Child - " << i+1 << endl 
			   << "---------" << endl
			   << object.children[i] << endl;
		
		return os;
	}
	
	/* Overloading == operator for comparing families */
	bool Family::operator == ( const Family& other ) const
	{
		bool children_are_same = true;
		
		if(numOfChildren == other.numOfChildren)
		{
			for(int i = 0; i < numOfChildren; ++i)
			{
				if(children[i] != other.children[i])
					children_are_same = false;
			}
		}
		
		else
		{
			children_are_same = false;
		}
		
		return (children_are_same && (mother == other.mother) && 
				(father == other.father));
	}
	
	/* Overloading [] operator. Returns with child at the given index */
	const Person& Family::operator [] ( int index ) const
	{
		if(index >= numOfChildren || index < 0)
		{
			cout << "Invalid index value!" << endl;
		}
			
		else
		{
			return children[index];
		}
	}
	
	/* Overloading + operator, forms a new family with first appropriate children */
	const Family Family::operator + ( const Family& other ) const
	{
		int index1 = -1, index2 = -1;

		for(int i = 0; i < numOfChildren && index1 == -1; ++i)
		{
			if(children[i].getSex() == 'F')
				index1 = i;
		}
			
		for(int i = 0; i < other.numOfChildren && index2 == -1; ++i)
		{
			if(other.children[i].getSex() == 'M')
				index2 = i;
		}
		
		if(index1 != -1 && index2 != -1)
		{
			return(Family(children[index1], other.children[index2]));
		}
		
		else
		{
			cout << "No match in children female-male." << endl;
			return *this;	/* Return this to avoid segmanttation fault error. */
		}
	}
	
	/* Overloading += operator, adds new child to the family */
	const Family& Family::operator += ( const Person& newChild )
	{
		Person temp[numOfChildren];
		
		for(int i = 0; i < numOfChildren; ++i)
			temp[i] = children[i];
		
		delete [] children;
		++numOfChildren;
		
		children = new Person [numOfChildren];
		
		for(int i = 0; i < numOfChildren - 1; ++i)
		{
			children[i] = temp[i];
		}
		
		/* Add the last child that is function parameter named 'newChild'. */
		children[numOfChildren - 1] = newChild;
		
		return *this;
	}
	
	/* Destructor */
	Family::~Family( )
	{
		delete [] children;
	}
}
/*######################################################################*/
/*						End of 101044017HW06Family.cpp					*/
/*######################################################################*/
