/*######################################################################*/
/*																		*/
/* 101044017HW06Person.cpp												*/
/*																		*/
/*----------------------------------------------------------------------*/
/*																		*/
/* Created by Necmeddin Said Karakoc on 25.11.2012						*/
/*																		*/
/*----------------------------------------------------------------------*/
/*																		*/
/* This source file includes definition of Person class.				*/
/*																		*/
/*######################################################################*/

/*######################################################################*/
/*								Includes								*/
/*######################################################################*/
#include "101044017HW06Person.h"

/*######################################################################*/
/*							Unnamed Namespace							*/
/*######################################################################*/
namespace
{
	/* Returns with upper case of the letter that is given as a function parameter. */
	char myToUpper( char ch )
	{
		if(ch >= 97)
			return (ch - 32);

		return ch;
	}
}

/*######################################################################*/
/*						Namespace of Karakoc							*/
/*######################################################################*/
namespace Karakoc
{
	/* Default constructor  */
	Person::Person( char gender, int birth ) : sex(myToUpper(gender)), birthyear(2012)
	{
		first_name = new char [1];
		last_name = new char [1];
		
		first_name[0] = '\0';
		last_name[0] = '\0';
	}
	
	/* Constructor that can take two strings, gender and year of birth */
	Person::Person( const char* first, const char* last, char gender, int birth)
					: sex(myToUpper(gender)), birthyear(birth)
	{
		int firstNameLen = strlen(first) + 1,	/* +1 for the null character */
			lastNameLen  = strlen(last) + 1;	/* +1 for the null character */
		
		first_name = new char [firstNameLen];
		last_name  = new char [lastNameLen];
		
		/* Also copy the null character for comparing two strings or	*/
		/* computing the length of a string.							*/
		for(int i = 0; i < firstNameLen; ++i)
			first_name[i] = first[i];
		
		for(int i = 0; i < lastNameLen; ++i)
			last_name[i] = last[i];
	}
	
	/* Copy constructor */
	Person::Person( const Person& other )
	{
		sex = other.sex;
		birthyear = other.birthyear;
		
		/* +1 for the null character */
		int firstNameLen = strlen(other.first_name) + 1,	
			lastNameLen  = strlen(other.last_name) + 1;
		
		first_name = new char [firstNameLen];
		last_name  = new char [lastNameLen];
			
		/* Also copy the null character for comparing two strings or	*/
		/* computing the length of a string.							*/
		for(int i = 0; i < firstNameLen; ++i)
			first_name[i] = other.first_name[i];

		for(int i = 0; i < lastNameLen; ++i)
			last_name[i] = other.last_name[i];
	}
	
	/* Overloading assignment operator */
	const Person& Person::operator = ( const Person& rightSide )
	{
		/* If the object that is parameter below is not me, start copying */
		if( this != &rightSide )
		{
			sex = rightSide.sex;
			birthyear = rightSide.birthyear;
			
			/* +1 for the null character */
			int firstNameLen = strlen(rightSide.first_name) + 1,	
				lastNameLen  = strlen(rightSide.last_name) + 1;
			
			delete [] first_name;
			delete [] last_name;

			first_name = new char [firstNameLen];
			last_name  = new char [lastNameLen];

			/* Also copy the null character for comparing two strings or	*/
			/* computing the length of a string.							*/
			for(int i = 0; i < firstNameLen; ++i)
				first_name[i] = rightSide.first_name[i];

			for(int i = 0; i < lastNameLen; ++i)
				last_name[i] = rightSide.last_name[i];
		}
		
		return *this;
	}
	
	/* Overloading stream extraction operator */
	istream& operator >> ( istream& is, Person& human )
	{
		char* temp;
		int i = 0;

		temp = new char [80];
		cout << "Enter the name >";
		
		is.getline(temp, 80);
		while(strlen(temp) >= 80)
		{
			cout << "Name length overflow. Enter smaller than 80 characters." << endl;
			cout << "Try again >";
			is.getline(temp, 80);
		}
		
		int first_size = strlen(temp) + 1;
		
		human.first_name = new char [first_size];
		
		for(int i = 0; i < first_size; ++i)
			human.first_name[i] = temp[i];
		
		cout << "Enter the last name >";
		
		is.getline(temp, 80);
		while(strlen(temp) >= 80)
		{
			cout << "Name length overflow. Enter smaller than 80 characters." << endl;
			cout << "Try again >";
			is.getline(temp, 80);
		}
		int last_size = strlen(temp) + 1;
		
		human.last_name = new char [last_size];
		
		for(i = 0; i < last_size; ++i)
			human.last_name[i] = temp[i];
		
		delete [] temp;
		
		char gender;
		cout << "Enter gender (m or f) >";
		is >> gender;
		gender = myToUpper(gender);
		
		while(gender != 'M' && gender != 'F')
		{
			cout << "Sorry. Try again >";
			is >> gender;
			gender = myToUpper(gender);
		}
		
		human.sex = gender;
		
		int birthyear;
		cout << "Enter birthyear >";
		is >> birthyear;
		
		while(birthyear <= 0)
		{
			cout << "Birthyear cannot be less than zero. Try again >";
			is >> birthyear;
		}
		
		human.birthyear = birthyear;
		
		return is;
	}
	
	/* Overloading stream insertion operator */
	ostream& operator << ( ostream& os, const Person& human )
	{
		os << "Name:      " << human.getFirstName() << endl
		   << "Last Name: " << human.getLastName() << endl
		   << "Gender:    ";
		
		if(human.sex == 'M')
			os << "Male" << endl;
		
		else 
			os << "Female" << endl;
		
		os << "Birthyear: " << human.birthyear << endl;
		return os;
	}
	
	/* Overloading == operator for comparing */
	bool Person::operator == ( const Person& other ) const
	{
		return (!strcmp(first_name, other.first_name) && 
				!strcmp(last_name, other.last_name) &&
				(sex == other.sex) && (birthyear == other.birthyear));
	}
	
	/* Destructor */
	Person::~Person( )
	{
		delete [] first_name;
		delete [] last_name;
	}
}
/*######################################################################*/
/*						End of 101044017HW06Person.cpp					*/
/*######################################################################*/
