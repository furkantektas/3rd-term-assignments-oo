/*######################################################################*/
/*																		*/
/* 101044017HW06Person.h												*/
/*																		*/
/*----------------------------------------------------------------------*/
/*																		*/
/* Created by Necmeddin Said Karakoc on 25.11.2012						*/
/*																		*/
/*----------------------------------------------------------------------*/
/*																		*/
/* This header file includes declaration of Person class.				*/
/*																		*/
/*######################################################################*/

/*######################################################################*/
/*								Includes								*/
/*######################################################################*/
#include <iostream>
#include <cstring>

/*######################################################################*/
/*							Namespaces Using							*/
/*######################################################################*/
using std::istream;
using std::ostream;
using std::cin;
using std::cout;
using std::endl;

#ifndef KARAKOC_H
#define	KARAKOC_H

/*######################################################################*/
/*							Unnamed Namespace							*/
/*######################################################################*/
namespace
{
	char myToUpper( char ch );
}

/*######################################################################*/
/*						Namespace of Karakoc							*/
/*######################################################################*/
namespace Karakoc
{
	class Person
	{
	public:
		Person( char gender = 'M', int birth = 2012 );
		Person( const char* first, const char* last , char gender = 'M', int birth = 2012);
		Person( const Person& other );
		const char* getFirstName( ) const { return first_name; }
		const char* getLastName( ) const { return last_name; }
		char getSex( ) const { return sex; }
		int getBirthyear( ) const { return birthyear; }
		const Person& operator = ( const Person& rightSide );
		friend istream& operator >> ( istream& is, Person& human );
		friend ostream& operator << ( ostream& os, const Person& human );
		bool operator == ( const Person& other ) const;
		bool operator != ( const Person& other ) const { return !(*this == other); }
		~Person( );
		
	private:
		char *first_name;
		char *last_name;
		char sex;
		int birthyear;
	};
}
#endif	/* KARAKOC_H */

/*######################################################################*/
/*						End of 101044017HW06Person.h					*/
/*######################################################################*/
