/*######################################################################*/
/*																		*/
/* 101044017HW06Family.h												*/
/*																		*/
/*----------------------------------------------------------------------*/
/*																		*/
/* Created by Necmeddin Said Karakoc on 25.11.2012						*/
/*																		*/
/*----------------------------------------------------------------------*/
/*																		*/
/* This header file includes declaration of Family class.				*/
/*																		*/
/*######################################################################*/

/*######################################################################*/
/*								Includes								*/
/*######################################################################*/
#include "101044017HW06Person.h"

#ifndef FAMILY_H
#define	FAMILY_H

/*######################################################################*/
/*						Namespace of Karakoc							*/
/*######################################################################*/
namespace Karakoc
{
	class Family
	{
	public:
		Family( );
		Family( const Person& mother, const Person& father );
		Family( const Family& other );
		const Family& operator = ( const Family& other );
		const Person& getMother( ) const { return mother; }
		const Person& getFather( ) const { return father; }
		const Person& getChild( int index ) const;
		int getNumOfChildren( ) const { return numOfChildren; }
		static int getNumOfFamilies( ) { return numOfFamilies; }
		friend istream& operator >> ( istream& is, Family& object );
		friend ostream& operator << ( ostream& os, const Family& object );
		bool operator == ( const Family& other ) const;
		bool operator != ( const Family& other ) const { return !(*this == other); };
		const Person& operator [] ( int index ) const;
		const Family operator + ( const Family& other ) const;
		const Family& operator += ( const Person& newChild );
		~Family( );
		
	private:
		Person mother;
		Person father;
		Person *children;
		int numOfChildren;
		static int numOfFamilies;
		
	};
}

#endif	/* FAMILY_H */

/*######################################################################*/
/*						End of 101044017HW06Family.h					*/
/*######################################################################*/
