/*######################################################################*/
/*																		*/
/* 101044017HW06Main.cpp												*/
/*																		*/
/*----------------------------------------------------------------------*/
/*																		*/
/* Created by Necmeddin Said Karakoc on 25.11.2012						*/
/*																		*/
/*----------------------------------------------------------------------*/
/*																		*/
/* This program includes class of Person and Family.					*/
/*																		*/
/*######################################################################*/

/*######################################################################*/
/*								Includes								*/
/*######################################################################*/
#include "101044017HW06Person.h"
#include "101044017HW06Family.h"

/*######################################################################*/
/*								Namespaces								*/
/*######################################################################*/
using std::cout;
using namespace Karakoc;

/*######################################################################*/
/*							Function Prototypes							*/
/*######################################################################*/
bool areRelative( const Person& p1, const Person& p2, const Family* families, int size );
bool isMemberOfFamily( const Person& person, const Family& family );
void test( );

/*######################################################################*/
/*							Main Function								*/
/*######################################################################*/
int main(int argc, char** argv)
{
	test();
	
	return 0;
}

/*######################################################################*/
/*						Function Implementations						*/
/*######################################################################*/
bool areRelative( const Person& p1, const Person& p2, const Family* families, int size )
{
	int i, j, k;

	for(i = 0; i < size; ++i)
	{
		if(isMemberOfFamily(p1, families[i]))
		{
			if(isMemberOfFamily(p2, families[i]))
			{
				return true;
			}
			
			else
			{
				for(j = 0; j < families[i].getNumOfChildren(); ++j)
				{
					for(k = 0; k < size; ++k)
					{
						if(families[i].getChild(j) == families[k].getMother() || 
						   families[i].getChild(j) == families[k].getFather())
						{
							if(isMemberOfFamily(p2, families[k]))
							{
								return true;
							}
						}
					}
				}
			}
		}
	}
	return false;
}

bool isMemberOfFamily( const Person& person, const Family& family )
{
	for(int i = 0; i < family.getNumOfChildren(); ++i)
	{
		if(person == family.getChild(i))
		{
			return true;
		}
	}
		
	if( person == family.getFather() || person == family.getMother() )
	{
		return true;
	}
	
	return false;
}

void test( )
{
	Person p1("f1-mother", "f1-last name", 'f', 1970), p2("f1-father", "f1-last name", 'm', 1965),
		   p3("f2-mother", "f2-last name", 'f', 1970), p4("f2-father", "f2-last name", 'm', 1965),
		   c1("c1", "lastName", 'm', 1992),
		   c2("c2", "lastName", 'f', 1991),
		   c3("c3", "lastName", 'm', 1990),
	       c4("c4", "lastName", 'f', 1994),
		   c5("c5", "lastName", 'm', 1992),
		   c6("c6", "lastName", 'f', 1991),
		   c7("c7", "lastName", 'm', 1990),
		   c8("c8", "lastName", 'f', 1994);
	
	Family f1(p1, p2), f2(p3, p4);
	
	f1 += c1;
	f1 += c2;
	f2 += c3;
	f2 += c4;
	
	Family families[3];
	
	families[0] = f1;
	families[1] = f2;
	families[2] = f1 + f2;
	families[2] += c6;
	families[2] += c7;
		
	cout << "-------------------------------------------" << endl;
	cout << "#           Testing Class of Person       #" << endl;
	cout << "-------------------------------------------" << endl;
	cout << "Test for getFirstName() | name of p1: " << p1.getFirstName() << endl;
	cout << "Test for getLastName()  | last name of p1: " << p1.getLastName() << endl;
	cout << "Test for getSex()       | gender of p1: " << p1.getSex() << endl;
	cout << "Test for getBirthyear() | birthyear of p1: " << p1.getBirthyear() << endl;
	cout << "Test for operator =     | p1 = p2" << endl;
	p1 = p2;
	cout << "Test for operator >>    | Enter information for p1:" << endl;
	cout << "Test for operator <<    | Information about p1: " << endl;
	cout << "Test for operator ==    |";
	
	if(p1 == p2)
		cout << " p1 == p2 is true" << endl;
	else
		cout << " p1 == p2 is false" << endl;
	
	cout << "Test for operator !=    |";
	
	if(p1 != p2)
		cout << " p1 != p2 is true" << endl;
	else
		cout << " p1 != p2 is false" << endl;
	
	cout << endl;
	cout << "-------------------------------------------" << endl;
	cout << "#           Testing Class of Family       #" << endl;
	cout << "-------------------------------------------" << endl;
	cout << "Test for getMother() | mother of f1: " << endl << endl << f1.getMother() << endl;
	cout << "Test for getFather() | father of f1: " << endl << endl << f1.getFather() << endl;
	cout << "Test for getChild() | 2.child of f1: " << endl << endl << f1.getChild(1) << endl;
	cout << "Test for getNumOfChildren() | number of children of f1: " << f1.getNumOfChildren() << endl;
	cout << "Test for getNumOfFamilies() | number of families: " << Family::getNumOfFamilies() << endl;
	cout << "Test for operator >> | Enter information for f1:" << endl;
	cout << "Test for operator <<        | Information about f1: " << endl << endl;
	cout << f1;
	cout << "Test for operator ==  |";
	if(f1 == f2)
		cout << " f1 == f2 is true" << endl;
	else
		cout << " f1 == f2 is false" << endl;
	
	cout << "Test for operator !=  |";
	
	if(f1 != f2)
		cout << " f1 != f2 is true" << endl;
	else
		cout << " f1 != f2 is false" << endl;
	
	cout << "Test for operator []  | f1[1] is:" << endl << endl << f1[1] << endl;
	cout << "Test for operator + | f1 + f2 : " << endl << endl << f1 + f2 << endl;
	
	cout << "Test for operator += | f1 += c5 f1:" << endl << endl;
	f1 += c5;
	cout << f1;
	
	cout << "Test for operator = | f1 = f2" << endl;
	f1 = f2;
	
	
	cout << "Test for global function named 'areRelative' | ";
	if(areRelative(c1, c6, families, 3))
	{
		cout << "c1 and c6 are relative." << endl;
	}
	
	else
	{
		cout << "c1 and c6 are not relative." << endl;
	}
	cout << "Test for stream extraction operators." << endl << endl;
	cout << "Enter information for person1: " << endl;
	cin >> p1;
	cin.get();
	cout << p1 << endl;
	cout << "Enter information for family1: " << endl;
	cin >> f1;
	cout << f1;
}
/*######################################################################*/
/*						End of 101044017HW06Main.cpp					*/
/*######################################################################*/
