/*
 * File:    Rectangle.java
 *
 * Course:  CSE241
 * Project: HW10
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw10;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author furkan
 */
public class Rectangle extends Polygon {

    public Rectangle() {
        super(4);
    }

    public Rectangle(Integer[] x, Integer[] y) {
        super(4, x, y);
    }

    public Rectangle(Integer[] x, Integer[] y, double thickness) {
        super(4, x, y, thickness);
    }

    @Override
    public void draw(Graphics g, double ratioX, double ratioY) {
        g.setColor(Color.YELLOW);
        super.draw(g, ratioX, ratioY);
    }
}
