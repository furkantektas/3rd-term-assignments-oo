/*
 * File:    Polygon.java
 *
 * Course:  CSE241
 * Project: HW10
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw10;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Scanner;

/**
 *
 * @author furkan
 */
public class Polygon implements Shape {

    private Integer[] _coordinatesX;
    private Integer[] _coordinatesY;
    private int _numOfEdge;
    private BasicStroke _strokeThickness;

    public Polygon() {
        setNumOfEdge(0);
        setCoordinatesX(new Integer[]{new Integer(0)});
        setCoordinatesY(new Integer[]{new Integer(0)});
        setStrokeThickness(1.0);
    }

    public Polygon(int numOfEdge) {
        setNumOfEdge(numOfEdge);
        setCoordinatesX(new Integer[]{new Integer(0)});
        setCoordinatesY(new Integer[]{new Integer(0)});
        setStrokeThickness(1.0);
    }

    public Polygon(int numOfEdge, Integer[] x, Integer[] y) {
        setNumOfEdge(numOfEdge);
        setCoordinatesX(x);
        setCoordinatesY(y);
        setStrokeThickness(1.0f);
    }

    public Polygon(int numOfEdge, Integer[] x, Integer[] y, double thickness) {
        setNumOfEdge(numOfEdge);
        setCoordinatesX(x);
        setCoordinatesY(y);
        setStrokeThickness(thickness);
    }

    @Override
    public void setCoordinatesX(Integer[] x) throws IllegalArgumentException {
        for (int coordinate : x) {
            if ((coordinate < 0) || (coordinate > Shapes.panelWidth)) {
                throw new ArrayIndexOutOfBoundsException("Invalid coordinates.");
            }
        }
        _coordinatesX = x;
    }

    @Override
    public void setCoordinatesY(Integer[] y) throws IllegalArgumentException {
        for (int coordinate : y) {
            if ((coordinate < 0) || (coordinate > Shapes.panelHeight)) {
                throw new ArrayIndexOutOfBoundsException("Invalid coordinates.");
            }
        }
        _coordinatesY = y;
    }

    /**
     * @return the _numOfEdge
     */
    public int getNumOfEdge() {
        return _numOfEdge;
    }

    /**
     * @param numOfEdge the _numOfEdge to set
     */
    public void setNumOfEdge(int numOfEdge) {
        this._numOfEdge = numOfEdge;
    }

    @Override
    public double getArea() {
        double area = 0.0;
        for (int i = 0; i < getNumOfEdge(); ++i) {
            area += (getXCoordinates()[i % getNumOfEdge()] *
                    getYCoordinates()[(i + 1) % getNumOfEdge()])
                    - (getXCoordinates()[(i + 1) % getNumOfEdge()] *
                    getYCoordinates()[i % getNumOfEdge()]);
        }
        return Math.abs(area / 2.0);
    }

    @Override
    public double getPerimeter() {
        double perimeter = 0.0;
        for (int i = 0; i < getNumOfEdge(); ++i) {
            perimeter +=
                    Math.sqrt(Math.pow(getXCoordinates()[(i + 1) % getNumOfEdge()] -
                    getXCoordinates()[i % getNumOfEdge()], 2) +
                    Math.pow(getYCoordinates()[(i + 1) % getNumOfEdge()] -
                    getYCoordinates()[i % getNumOfEdge()], 2));
        }
        return perimeter;
    }

    @Override
    public Integer[] getXCoordinates() {
        return _coordinatesX;
    }

    @Override
    public String listCoordinatesX() {
        String str = "";
        for (int coordinate : _coordinatesX) {
            str += coordinate + ", ";
        }
        return str;
    }

    @Override
    public Integer[] getYCoordinates() {
        return _coordinatesY;
    }

    @Override
    public String listCoordinatesY() {
        String str = "";
        for (int coordinate : _coordinatesY) {
            str += coordinate + ", ";
        }
        return str;
    }

    /**
     * Gets instance variables from user and calls their setters.
     *
     * @throws RuntimeException
     */
    @Override
    public void ParametersFromUser() throws RuntimeException {
        if (getNumOfEdge() <= 0) {
            throw new RuntimeException("Invalid num of edge.");
        }
        Scanner sc = new Scanner(System.in);

        Integer[] xCoordinates = new Integer[getNumOfEdge()];
        Integer[] yCoordinates = new Integer[getNumOfEdge()];

        for (int i = 0; i < getNumOfEdge(); ++i) {
            System.out.printf("Enter the x coordinate for point %d => ", i);
            xCoordinates[i] = sc.nextInt();

            System.out.printf("Enter the y coordinate for point %d => ", i);
            yCoordinates[i] = sc.nextInt();
        }

        System.out.printf("Enter the thickness of the lines => ");
        setStrokeThickness(sc.nextDouble());

        setCoordinatesX(xCoordinates);
        setCoordinatesY(yCoordinates);
    }

    /**
     *
     * Draws a polygon according to the instance variables.Polygons' coordinates
     * and line thicknesses can be resizable.
     *
     * @param g
     * @param ratioX current window width/default window width
     * @param ratioY current window height/default window height
     * @throws ArrayIndexOutOfBoundsException
     */
    @Override
    public void draw(Graphics g, double ratioX, double ratioY)
            throws ArrayIndexOutOfBoundsException {

        Graphics2D g2d = (Graphics2D) g;

        int[] xCoordinates = new int[getXCoordinates().length];

        for (int i = 0; i < xCoordinates.length; ++i) {
            xCoordinates[i] = (int) (getXCoordinates()[i] * ratioX);
        }

        int[] yCoordinates = new int[getYCoordinates().length];

        for (int i = 0; i < yCoordinates.length; ++i) {
            yCoordinates[i] = (int) (getYCoordinates()[i] * ratioY);
        }

        g2d.setPaintMode();
        g2d.setStroke(new BasicStroke(
                    (float) (_strokeThickness.getLineWidth() * ratioY * ratioX))
                );
        g.drawPolygon(xCoordinates, yCoordinates, getNumOfEdge());


    }

    @Override
    public void setStrokeThickness(double thickness) {
        _strokeThickness = new BasicStroke((float) thickness);
    }
}
