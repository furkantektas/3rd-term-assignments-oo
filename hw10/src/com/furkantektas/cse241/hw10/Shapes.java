/*
 * File:    Shapes.java
 *
 * Course:  CSE241
 * Project: HW10
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw10;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author furkan
 */
public class Shapes extends JPanel {

//    private Shape[] _shapeList;
    private ArrayList<Shape> _shapeList = new ArrayList<Shape>();
    public static final int panelWidth = 650;
    public static final int panelHeight = 400;

    public Shapes() {
        this.setPreferredSize(new Dimension(panelWidth, panelHeight));
    }

    public void addShape(Shape newShape) {
        _shapeList.add(newShape);
    }

    public void removeShape(int index) throws IndexOutOfBoundsException {
        _shapeList.remove(index);
    }

    public void listShapes() {
        int i = 1;
        for (Shape shp : _shapeList) {
            System.out.println(i + ") "
                    + shp.getClass().getSimpleName()
                    + "(" + shp.listCoordinatesX() + ")"
                    + "  (" + shp.listCoordinatesY() + ")");
            ++i;
        }
    }

    public Shape getShape(int index) throws IndexOutOfBoundsException {
        return _shapeList.get(index);
    }

    @Override
    public void paintComponent(Graphics g) {

        double ratioX = (double) getWidth() / panelWidth;
        double ratioY = (double) getHeight() / panelHeight;

        super.paintComponent(g);
        shapeLoop:
        for (Shape shape : _shapeList) {
            try {
                shape.draw(g, ratioX, ratioY);
            } catch (Exception e) {
                //displaying error message with the color of wrong shape
                g.drawString(e.getMessage(), 25, 25);
                _shapeList.remove(shape); // removing the invalid shape
                System.out.println("Invalid Object: "
                        + shape.getClass().getSimpleName()
                        + "\nShape was removed.");
                break shapeLoop;
            }
        }
    }
}
