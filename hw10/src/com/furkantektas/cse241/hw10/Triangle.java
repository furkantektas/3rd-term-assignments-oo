/*
 * File:    Triangle.java
 *
 * Course:  CSE241
 * Project: HW10
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw10;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author furkan
 */
public class Triangle extends Polygon {

    public Triangle() {
        super(3);
    }

    public Triangle(Integer[] x, Integer[] y) {
        super(3, x, y);
    }

    public Triangle(Integer[] x, Integer[] y, double thickness) {
        super(3, x, y, thickness);
    }

    @Override
    public void draw(Graphics g, double ratioX, double ratioY) {
        g.setColor(Color.GREEN);
        super.draw(g, ratioX, ratioY);
    }
}
