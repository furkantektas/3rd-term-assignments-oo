/*
 * File:    HW10_111044029Main.java
 *
 * Course:  CSE241
 * Project: HW10
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw10;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author furkan
 */
public class HW10_111044029Main {

    public static final int FRAMEWIDTH = Shapes.panelWidth;
    public static final int FRAMEHEIGHT = Shapes.panelHeight;
    public static final String[] ShapeNames = {"Ellipse", "Circle", "Triangle",
        "Rectangle", "Hexagon"};

    public static void main(String[] args) {
        Shapes shapeList = new Shapes();

        JFrame mainFrame = new JFrame(); // creates a new JFrame
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setTitle("111044029 - HW10");
        shapeList.setBackground(Color.WHITE);
        shapeList.setForeground(Color.RED);

        try {

            // Creating some random shapes with random stroke width

            Ellipse e = new Ellipse(
                    getRandomInt(FRAMEHEIGHT - 300),
                    getRandomInt(FRAMEHEIGHT - 300),
                    getRandomCoordinates(FRAMEWIDTH - 300),
                    getRandomCoordinates(FRAMEHEIGHT - 300),
                    (getRandomCoordinates(20)[0] / 5.0));

            Circle c1 = new Circle(
                    getRandomInt(FRAMEHEIGHT - 300),
                    getRandomCoordinates(FRAMEWIDTH - 300),
                    getRandomCoordinates(FRAMEHEIGHT - 300),
                    (getRandomCoordinates(20)[0] / 5.0));

            Circle c2 = new Circle(
                    getRandomInt(FRAMEHEIGHT - 300),
                    getRandomCoordinates(FRAMEWIDTH - 300),
                    getRandomCoordinates(FRAMEHEIGHT - 300),
                    (getRandomCoordinates(20)[0] / 5.0));

            Triangle t = new Triangle(
                    getRandomCoordinates(FRAMEWIDTH),
                    getRandomCoordinates(FRAMEHEIGHT),
                    (getRandomCoordinates(20)[0] / 5.0));

            Hexagon h = new Hexagon(
                    getRandomCoordinates(FRAMEWIDTH),
                    getRandomCoordinates(FRAMEHEIGHT),
                    (getRandomCoordinates(20)[0] / 5.0));

            Rectangle r = new Rectangle(
                    getRandomCoordinates(FRAMEWIDTH),
                    getRandomCoordinates(FRAMEHEIGHT),
                    (getRandomCoordinates(20)[0] / 5.0));

            shapeList.addShape(e);
            shapeList.addShape(c1);
            shapeList.addShape(c2);
            shapeList.addShape(h);
            shapeList.addShape(t);
            shapeList.addShape(r);

        } catch (Exception exp) {
            JLabel errorLabel = new JLabel(exp.getMessage());
            mainFrame.add(errorLabel, SwingConstants.CENTER);
            System.out.println(exp.getMessage());
        } finally {
            mainFrame.setContentPane(shapeList);
            mainFrame.pack();
            centerFrame(mainFrame);
            mainFrame.setVisible(true); // show the frame

            while (displayMenu(shapeList) != 6) {
                mainFrame.repaint();
            }
        }
        System.out.println("Good bye!");
        mainFrame.dispose();
    }

    public static Integer[] getRandomCoordinates(int max) {
        Random r = new Random(564651L);
        return new Integer[]{
                    getRandomInt(max),
                    getRandomInt(max),
                    getRandomInt(max),
                    getRandomInt(max),
                    getRandomInt(max),
                    getRandomInt(max),
                    getRandomInt(max),
                    getRandomInt(max),
                    getRandomInt(max),};
    }

    public static int getRandomInt(int max) {
        Random r = new Random();
        return Math.abs(r.nextInt(max));

    }

    public static void centerFrame(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }

    public static int displayMenu(Shapes shapeList) {
        System.out.println("||||||||| ~ MAIN MENU ~ |||||||||");
        System.out.println("1) Create shape");
        System.out.println("2) Manage shapes");
        System.out.println("3) List shapes");
        System.out.println("4) Remove shapes");
        System.out.println("5) Get more info about shapes");
        System.out.println("6) Exit");

        Scanner sc = new Scanner(System.in);

        try {
            int userChoice = sc.nextInt();

            switch (userChoice) {
                case 1:
                    createShape(shapeList);
                    break;
                case 2:
                    manageShapes(shapeList);
                    break;
                case 3:
                    shapeList.listShapes();
                    break;
                case 4:
                    removeShape(shapeList);
                    break;
                case 5:
                    infoShapes(shapeList);
                    break;
                case 6:
                    return 6;
                default:
                    System.out.println("Invalid choice! Try again.");
            }
            return userChoice;
        } catch (InputMismatchException e) {
            System.out.println("Invalid choice! Try again.");
            return -1;
        }
    }

    public static void listShapeTypes() {
        int i = 1;
        for (String shapeName : ShapeNames) {
            System.out.println(i + ") " + shapeName);
            ++i;
        }
    }

    public static void createShape(Shapes myShape) {
        System.out.println("||||||||| ~ CREATE A SHAPE ~ |||||||||");

        Scanner sc = new Scanner(System.in);
        int userChoice = -1;

        do {
            listShapeTypes();
            userChoice = sc.nextInt();
            if (userChoice < 0 || userChoice > ShapeNames.length) {
                System.out.println("Invalid choice! Try again");
            }

        } while (userChoice < 0 || userChoice > ShapeNames.length);

        Shape tempShape;

        switch (userChoice) {
            case 1:
                tempShape = new Ellipse();
                break;
            case 2:
                tempShape = new Circle();
                break;
            case 3:
                tempShape = new Triangle();
                break;
            case 4:
                tempShape = new Rectangle();
                break;
            case 5:
                tempShape = new Hexagon();
                break;
            default:
                tempShape = new Rectangle();
        }
        try {
            tempShape.ParametersFromUser();
            myShape.addShape(tempShape);
        } catch (IndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void manageShapes(Shapes myShape) {
        System.out.println("||||||||| ~ MANAGE SHAPES ~ |||||||||");
        myShape.listShapes();
        System.out.println("Pick a shape => ");
        Scanner sc = new Scanner(System.in);
        myShape.getShape(sc.nextInt() - 1).ParametersFromUser();
    }

    public static void removeShape(Shapes myShape) {
        System.out.println("||||||||| ~ REMOVE A SHAPE ~ |||||||||");
        myShape.listShapes();

        Scanner sc = new Scanner(System.in);
        int userChoice = sc.nextInt() - 1;

        try {
            myShape.removeShape(userChoice);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Invalid index! No shape was removed.");
        }
    }

    public static void infoShapes(Shapes myShape) {
        System.out.println("||||||||| ~ SHAPE INFORMATION ~ |||||||||");
        myShape.listShapes();

        Scanner sc = new Scanner(System.in);
        int userChoice = sc.nextInt() - 1;

        try {
            Shape temp = myShape.getShape(userChoice);

            System.out.println("Shape       : " + temp.getClass().getSimpleName());
            System.out.println("  Area      : " + temp.getArea());
            System.out.println("  Perimeter : " + temp.getPerimeter());
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Invalid index!");
        }
    }
}