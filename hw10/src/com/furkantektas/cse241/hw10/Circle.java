/*
 * File:    Circle.java
 *
 * Course:  CSE241
 * Project: HW10
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on December 25, 2012, 8:45 PM
 */
package com.furkantektas.cse241.hw10;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Scanner;

/**
 *
 * @author furkan
 */
public class Circle extends Ellipse {

    public Circle() {
        super();
    }

    public Circle(int a, Integer[] x, Integer[] y) {
        super(a, a, x, y);
    }

    public Circle(int a, Integer[] x, Integer[] y, double thickness) {
        super(a, a, x, y, thickness);
    }

    @Override
    public void draw(Graphics g, double ratioX, double ratioY) {
        g.setColor(Color.cyan);
        super.draw(g, ratioX, ratioY);
    }

    @Override
    public void ParametersFromUser() throws RuntimeException {
        Scanner sc = new Scanner(System.in);
        int temp;

        System.out.print("Enter the radius of circle => ");
        temp = sc.nextInt() * 2; // height is the 2*radius
        setWidth(temp);
        setHeight(temp);

        Integer[] tempCoordinates = new Integer[1];

        System.out.print("Enter the topmost coordinate for circle => ");
        tempCoordinates[0] = sc.nextInt();
        setCoordinatesX(tempCoordinates);

        System.out.print("Enter the leftmost coordinate for circle => ");
        tempCoordinates[0] = sc.nextInt();
        setCoordinatesY(tempCoordinates);

        System.out.printf("Enter the thickness of the lines => ");
        setStrokeThickness(sc.nextDouble());
    }
}
