/*
 * File:    111044029HW04.cpp
 *
 * Course:  CSE241
 * Project: HW05
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 1, 2012, 5:47 PM
 * Last updated on November 18, 2012, 22:40 PM
 * 
 */

#include<iostream>

#ifndef SERIESOFPARTIALSUMS_H
#include "111044029HW05SeriesOfPartialSums.h"
#endif


int main(int argc, char** argv) {
    using std::endl;
    using std::cout;
    using std::boolalpha;
    using std::cin;

    cout << "Constant objects, constructor and binary operators:\n";
        SeriesOfPartialSums f1(3);
        cout << "f1 => " << f1 << endl;
        const SeriesOfPartialSums f2(5);
        cout << "f2 => " << f2 << endl;
        const SeriesOfPartialSums f3 = f1+f2;
        cout << "f3 => " << f3 << endl;
        const SeriesOfPartialSums f4 = f1-f2;
        cout << "f4 => " << f4 << endl;
    
    cout << "Stream insertion & extraction operators\n";
        cout << "New K value for f1 >> ";
        cin >> f1;
        cout << "New f1 => " << f1 << endl;
    
    cout << "Boolean Expressions\n";
        
        cout << "f1 == f2    => " << boolalpha << (f1 == f2) << endl;
        cout << "f1 == f1    => " << boolalpha << (f1 == f1) << endl;
        cout << "!(f1 != f1) => " << boolalpha << !(f1 != f1) << endl;
        cout << "f1 != f2    => " << boolalpha << (f1 != f2) << endl;
        cout << "f1<f2       => " << boolalpha << (f1 < f2) << endl;
        cout << "f1>f2       => " << boolalpha << (f1 > f2) << endl;
        cout << "f1<=f2      => " << boolalpha << (f1 <= f2) << endl;
        cout << "f1>=f2      => " << boolalpha << (f1 >= f2) << endl;

    unsigned int i;
    cout << "Enter i for getting f[i] =>";
    cin >> i;
    cout << "f1[" << i << "] => " << f1[i] << endl;

    cout << "Compound assignment tests\n";
        cout << "f1 =>   " << f1 << endl;
        cout << "f2 =>   " << f2 << endl;
        cout << "f1 += f2 => " << (f1 += f2) << endl; // problem
        cout << "f1 -= f2 => " << (f1 -= f2) << endl;
        cout << "f1 *= f2 => " << (f1 *= f2) << endl;
        cout << "f1 /= f2 => " << (f1 /= f2) << endl;

    cout << "Unary operators\n";
        cout << "f1 =>   " << f1 << endl;
        f1++;
        cout << "f1++ => " << f1 << endl;
        f1--;
        cout << "f1-- => " << f1 << endl;
        ++f1;
        cout << "++f1 => " << f1 << endl;
        --f1;
        cout << "--f1 => " << f1 << endl;
        f1 = -f1;
        cout << "-f1  => " << f1 << endl;

    return 0;
}