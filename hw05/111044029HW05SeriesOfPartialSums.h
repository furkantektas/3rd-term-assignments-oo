/*
 * File:    111044029HW04SeriesOfPartialSums.h
 *
 * Course:  CSE241
 * Project: HW05
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 1, 2012, 5:47 PM
 * Last updated on November 18, 2012, 22:40 PM
 */

#ifndef SERIESOFPARTIALSUMS_H
#define	SERIESOFPARTIALSUMS_H


#include<iostream> // for i/ostream
#include<vector> // for vectors

using std::vector;
using std::ostream;
using std::istream;

class SeriesOfPartialSums {
public:

    SeriesOfPartialSums(int limit = 0, double term = 1.0/2.0);

    inline int getLimitK() const;

    inline const double& get(int index) const;
    // returns the index-th element of series

    static unsigned int getCounter();

    void setLimitK(int inpLimit);

    double evaluate () const;
    // evaluates the sum of all vector sum elements

    bool isSmaller(const SeriesOfPartialSums &otherSeries) const;
    // takes another series object as parameter and returns true
    // if the other series evaluates to a larger number

    friend void Test(SeriesOfPartialSums &sampleSeries);
    // friend function to test private function and access private data members

    SeriesOfPartialSums& sumElement(const unsigned int i, double value);
    
    // binary operators
    SeriesOfPartialSums operator+(const SeriesOfPartialSums& rValue) const;
    SeriesOfPartialSums operator-(const SeriesOfPartialSums& rValue) const;
    SeriesOfPartialSums operator*(const SeriesOfPartialSums& rValue) const;
    SeriesOfPartialSums operator/(const SeriesOfPartialSums& rValue) const;
    
    // unary minus operator
    SeriesOfPartialSums operator-() const;

    // compound assignment operators
    const SeriesOfPartialSums& operator-=(const SeriesOfPartialSums& rValue);
    const SeriesOfPartialSums& operator+=(const SeriesOfPartialSums& rValue);
    const SeriesOfPartialSums& operator*=(const SeriesOfPartialSums& rValue);
    const SeriesOfPartialSums& operator/=(const SeriesOfPartialSums& rValue); 

    // index operator
    const double& operator[](const unsigned int ind) const;
    double& operator[](const unsigned int ind);

    // post increment/decrement operators
    SeriesOfPartialSums operator++(const int trick);    
    SeriesOfPartialSums operator--(const int trick);    

    // pre increment/decrement operators
    SeriesOfPartialSums& operator++();    
    SeriesOfPartialSums& operator--();    

    // logical operators
    bool operator<(const SeriesOfPartialSums& rValue) const;    
    bool operator>(const SeriesOfPartialSums& rValue) const;    
    bool operator<=(const SeriesOfPartialSums& rValue) const;    
    bool operator>=(const SeriesOfPartialSums& rValue) const;    
    bool operator==(const SeriesOfPartialSums& rValue) const;    
    bool operator!=(const SeriesOfPartialSums& rValue) const; 

    // i/ostream functions   
    friend std::ostream& operator<<(std::ostream& stream, const SeriesOfPartialSums& rValue);
    // stream insertion operator will output the element of series as more readable
    // format like 1/2. If the elements are getting smaller or cant be defined as a/b
    // scientific notation is preffered for numerator and denominator.

    friend std::istream& operator>>(std::istream& stream, SeriesOfPartialSums& rValue);

private:
    int limitK; // k
    double mainTerm; //an
    vector<double> sum; // all an's from an^0 to an^k
    static unsigned int counter; // SeriesOfPartalSums objects counter

    void add(double value);
    // pushes value to the end of the vector sum

    void remove();
    // removes an element from end of the vector sum

    void mult(double constant);
    // multiplies main term and refills the vector with new mainTerm

    void validateK(int k)  const;
    // terminates program if the k is negative

    void validateIndex(int ind) const;
    // terminates program for invalid index

    inline bool isEmpty() const;
    // returns true if limit is zero

    void fillSeries();
    // fills the sum vector

    int gcd(int num1, int num2) const;
    // calculates the greatest common divisor

    static unsigned int increaseCounter();
    // increases the SeriesOfPartalSums objects counter
};
#endif	/* SERIESOFPARTIALSUMS_H */