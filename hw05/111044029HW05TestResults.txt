Constant objects, constructor and binary operators:
f1 => 1  1/2  1/4  
f2 => 1  1/2  1/4  1/8  1/16  
f3 => 2  1  1/2  1/8  1/16  
f4 => 0  0  0  -1/8  -1/16  
Stream insertion & extraction operators
New K value for f1 >> New f1 => 1  1/2  1/4  1/8  1/16  1/32  
Boolean Expressions
f1 == f2    => false
f1 == f1    => true
!(f1 != f1) => true
f1 != f2    => true
f1<f2       => false
f1>f2       => true
f1<=f2      => false
f1>=f2      => true
Enter i for getting f[i] =>f1[5] => 0.03125
Compound assignment tests
f1 =>   1  1/2  1/4  1/8  1/16  1/32  
f2 =>   1  1/2  1/4  1/8  1/16  
f1 += f2 => 2  1  1/2  1/4  1/8  1/32  
f1 -= f2 => 1  1/2  1/4  1/8  1/16  1/32  
f1 *= f2 => 1  1/4  1/16  1/64  1/256  1/32  
f1 /= f2 => 1  1/2  1/4  1/8  1/16  1/32  
Unary operators
f1 =>   1  1/2  1/4  1/8  1/16  1/32  
f1++ => 2  3/2  5/4  9/8  2.0752e+15/1953125  2.01416e+15/1953125  
f1-- => 1  1/2  1/4  1/8  1/16  1/32  
++f1 => 2  3/2  5/4  9/8  2.0752e+15/1953125  2.01416e+15/1953125  
--f1 => 1  1/2  1/4  1/8  1/16  1/32  
-f1  => -1  -1/2  -1/4  -1/8  -1/16  -1/32  
