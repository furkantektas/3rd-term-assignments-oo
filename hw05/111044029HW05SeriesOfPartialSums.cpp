/*
 * File:    111044029HW05SeriesOfPartialSums.cpp
 *
 * Course:  CSE241
 * Project: HW05
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 1, 2012, 5:47 PM
 * Last updated on November 18, 2012, 22:40 PM
 */

#ifndef SERIESOFPARTIALSUMS_H
#include "111044029HW05SeriesOfPartialSums.h"
#endif

#include<iostream> // for cout
#include<cstdlib> // for exit
#include<vector> // for vectors
#include<cmath> // for pow

using std::cout;
using std::exit;
using std::vector;
using std::pow;

SeriesOfPartialSums::SeriesOfPartialSums(int limit, double term) : mainTerm(term)
{
    setLimitK(limit);
    ++SeriesOfPartialSums::counter;
}

void SeriesOfPartialSums::setLimitK(int inpLimit) {
    validateK(inpLimit);
    limitK = inpLimit;
    // updating series
    fillSeries();
}

inline int SeriesOfPartialSums::getLimitK() const {
    return limitK;
}

inline const double& SeriesOfPartialSums::get(int index) const {
    validateIndex(index);
    return sum.at(index);
}

double SeriesOfPartialSums::evaluate () const
{
    double total = 0.0;
    if(!isEmpty())
        for(unsigned int i=0;i<sum.size();++i)
            total += sum[i];
    return total;
}

bool SeriesOfPartialSums::isSmaller(const SeriesOfPartialSums &otherSeries) const
{
    return (evaluate() < otherSeries.evaluate());
}

unsigned int SeriesOfPartialSums::getCounter(){
    return counter;
}

inline bool SeriesOfPartialSums::isEmpty() const {
    return (getLimitK() == 0);
}

void SeriesOfPartialSums::fillSeries() {
    sum.clear();
    for(unsigned int i=0;i<getLimitK();++i)
        add(pow(mainTerm,i));
}

int SeriesOfPartialSums::gcd(int num1, int num2) const{
    if(num2 == 0)
        return num1;
    gcd(num2, num1%num2);
}


void SeriesOfPartialSums::add(double value)
{
    sum.push_back(value);
}

void SeriesOfPartialSums::remove()
{
    if(sum.size() > 0)
        sum.pop_back();
}

void SeriesOfPartialSums::mult(double constant)
{
    for(unsigned int i=0; i<sum.size(); ++i)
        sum[i] *= constant;
}

void SeriesOfPartialSums::validateK(int k)  const
{
    if(k < 0)
    {
        cout << "Invalid limit K. Terminating.\n";
        exit(1);
    }
}

void SeriesOfPartialSums::validateIndex(int ind) const
{
    if( (ind < 0) || (ind >= getLimitK()) )
    {
        cout << "Invalid index. Note that maximum index can be k-1. Terminating.\n";
        exit(1);
    }
}

SeriesOfPartialSums& SeriesOfPartialSums::sumElement(const unsigned int index, double value)
{
    validateIndex(index);
    sum[index] += value;
}

// Operator Overloads

// binary operators
SeriesOfPartialSums SeriesOfPartialSums::operator+(const SeriesOfPartialSums& rValue) const
{
    //unsigned const int maxK = (getLimitK() >= rValue.getLimitK()) ? 
    //                           getLimitK() : rValue.getLimitK();
    unsigned const int maxK = (sum.size() >= rValue.sum.size()) ? 
                               sum.size() : rValue.sum.size();
    SeriesOfPartialSums temp;
    double lElement, rElement;
    for(unsigned int i=0; i < maxK; ++i)
    {
        // assume 0 if the element of series is not defined.
        lElement = (i < sum.size()) ? sum.at(i) : 0.0;
        rElement = (i < rValue.sum.size()) ? rValue.sum.at(i) : 0.0;
        // sum and append the temp Series
        temp.add(lElement+rElement);
    }
    temp.limitK = maxK;
    return temp;
}

// PreCondition: Uses unary minus operator
SeriesOfPartialSums SeriesOfPartialSums::operator-(const SeriesOfPartialSums& rValue) const
{
    SeriesOfPartialSums temp = *this + (-rValue);
    return temp;
}

SeriesOfPartialSums SeriesOfPartialSums::operator*(const SeriesOfPartialSums& rValue) const
{
    unsigned const int maxK = (sum.size() >= rValue.sum.size()) ? 
                               sum.size() : rValue.sum.size();
    SeriesOfPartialSums temp;
    double lElement, rElement;
    for(unsigned int i=0; i<maxK; ++i)
    {
        // assume 1 if the element of series is not defined.
        lElement = (i < sum.size()) ? sum.at(i) : 1.0;
        rElement = (i < rValue.sum.size()) ? rValue.sum.at(i) : 1.0;

        // sum and append the temp Series
        temp.add(lElement*rElement);
    }
    temp.limitK = maxK;
    return temp;
}

SeriesOfPartialSums SeriesOfPartialSums::operator/(const SeriesOfPartialSums& rValue) const
{
    unsigned const int maxK = (sum.size() >= rValue.sum.size()) ? 
                               sum.size() : rValue.sum.size();
    SeriesOfPartialSums temp;
    double lElement, rElement;
    for(unsigned int i=0; i<maxK; ++i)
    {
        // assume 1 if the element of series is not defined.
        lElement = (i < sum.size()) ? sum.at(i) : 1.0;
        rElement = (i < rValue.sum.size()) ? rValue.sum.at(i) : 1.0;

        // sum and append the temp Series
        temp.add(lElement/rElement);
    }
    temp.limitK = maxK;
    return temp;
}

// unary operators
SeriesOfPartialSums SeriesOfPartialSums::operator-() const
{
    SeriesOfPartialSums temp = *this; // copying current object to temp object
    temp.mult(-1.0); // all of the elements are multiplied with -1
    return temp;
}

// compound assignment operators
const SeriesOfPartialSums& SeriesOfPartialSums::operator-=(const SeriesOfPartialSums& rValue)
{
    *this = (*this - rValue);
    return *this;
}

const SeriesOfPartialSums& SeriesOfPartialSums::operator+=(const SeriesOfPartialSums& rValue)
{
    *this = (*this + rValue);
    return *this;
}

const SeriesOfPartialSums& SeriesOfPartialSums::operator*=(const SeriesOfPartialSums& rValue)
{
    *this = (*this * rValue);
    return *this;
}

const SeriesOfPartialSums& SeriesOfPartialSums::operator/=(const SeriesOfPartialSums& rValue)
{
    *this = (*this / rValue);
    return *this;
}

// index operator
//for const objects
const double& SeriesOfPartialSums::operator[](const unsigned int ind) const
{
    return get(ind);
}

//for non-const objects
double& SeriesOfPartialSums::operator[](const unsigned int ind)
{
    validateIndex(ind);
    return sum.at(ind);
}

// post increment/decrement operators
SeriesOfPartialSums& SeriesOfPartialSums::operator++()
{
    for(unsigned int i=0; i< getLimitK(); ++i)
        sum.at(i) += 1.0;
    return *this;
}

SeriesOfPartialSums& SeriesOfPartialSums::operator--()
{
    for(unsigned int i=0; i< getLimitK(); ++i)
        sum.at(i) -= 1.0;
    return *this;
}

// pre increment/decrement operators
SeriesOfPartialSums SeriesOfPartialSums::operator++(const int trick)
{
    SeriesOfPartialSums temp = *this;
    for(unsigned int i=0; i< getLimitK(); ++i)
        sum.at(i) += 1.0;
    return temp;
}

SeriesOfPartialSums SeriesOfPartialSums::operator--(const int trick)  
{
    SeriesOfPartialSums temp = *this;
    for(unsigned int i=0; i< getLimitK(); ++i)
        sum.at(i) -= 1.0;
    return temp;
}


// logical operators
bool SeriesOfPartialSums::operator<(const SeriesOfPartialSums& rValue) const
{
    return (evaluate() < rValue.evaluate());
}

bool SeriesOfPartialSums::operator>(const SeriesOfPartialSums& rValue) const    
{
    return (evaluate() > rValue.evaluate());
}

bool SeriesOfPartialSums::operator<=(const SeriesOfPartialSums& rValue) const    
{
    return (evaluate() <= rValue.evaluate());
}

bool SeriesOfPartialSums::operator>=(const SeriesOfPartialSums& rValue) const    
{
    return (evaluate() >= rValue.evaluate());
}

bool SeriesOfPartialSums::operator==(const SeriesOfPartialSums& rValue) const    
{
    bool equal = true;
    if(getLimitK() != rValue.getLimitK())
        return false;
    for(unsigned int i=0; equal && (i < getLimitK()); ++i)
        equal = (sum[i] == rValue.sum[i]);
        // == operator is used on double because
        // builtin C++ operator == for doubles produced correct results
        // source: http://stackoverflow.com/a/77735
    return equal;
}

bool SeriesOfPartialSums::operator!=(const SeriesOfPartialSums& rValue) const
{
    return !(*this == rValue);
}


// i/ostream functions   
std::ostream& operator<<(std::ostream& stream, const SeriesOfPartialSums& series)
{
    for(unsigned int i=0; i < series.sum.size(); ++i)
    {
        double num = series.get(i);
        unsigned long int fractionLength = 1000000000UL; // enough for 1/512
        int commonDivisor, denom;
        
        do {
            num *= fractionLength;
            commonDivisor = series.gcd(num, fractionLength);
        } while( (num/commonDivisor) > 10);

        if(fractionLength/commonDivisor == 1) // no need to put '/' character
            stream << num / commonDivisor << "  ";
        else
        {
            // handling the negative numbers
            if(commonDivisor<0)
                commonDivisor *= -1;
            stream << num / commonDivisor << "/" << fractionLength / commonDivisor
                 << "  ";
        }
    }
    return stream;
}

// stream insertion operator can be used for setting new limitK so that
// series will be updated.
std::istream& operator>>(std::istream& stream, SeriesOfPartialSums& series)
{
    unsigned int kValue;
    stream >> kValue;
    series.setLimitK(kValue);
    return stream;
}

// initializing counter by value 0
unsigned int SeriesOfPartialSums::counter = 0;