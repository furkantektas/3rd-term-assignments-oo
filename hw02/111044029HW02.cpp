/*
 * File:    111044029HW01.cpp
 *
 * Course:  CSE241
 * Project: HW02
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on October 15, 2012, 7:18 AM
 */

#include <iostream>
#include "111044029HW02.h"

#define NDEBUG

using namespace std;

/*
 *
 */
int main(int argc, char** argv) {
    char userInput[STRSIZE];
    if(argc == 2)
    {
        cout << "Word: " << argv[1] << endl;
        strCpy(argv[1],userInput);
    }

    int goOn = -1;
    do
    {
        if(argc != 2)
            getWord(userInput);
        if(checkRoot(userInput) == NOTFOUND)
            cout << "No root found!\n";
        checkSuffix(userInput,findWithoutSuffix,"Without ",WITHOUTSUFFIXINDEX);
        checkSuffix(userInput,findPluralSuffix,"Plural",PLURALSUFFIXINDEX);
        checkSuffix(userInput,findMoveAwaySuffix,"Movement away",MOVEAWAYSUFFIXINDEX);
        checkSuffix(userInput,findStaticPosSuffix,"Static position",STATICPOSSUFFIXINDEX);
        checkSuffix(userInput,findOwnershipSuffix,"Ownership",OWNERSUFFIXINDEX);
        checkSuffix(userInput,findObjectSuffix,"Object",OBJECTSUFFIXINDEX);
        checkSuffix(userInput,findMoveToSuffix,"Movement towards",MOVETOSUFFIXINDEX);

        if(argc != 2)
        {
            cout << "Go on?\n\t[1]: Yes\t[2]: No\n";
            cin >> goOn;
        }

    } while(goOn == 1);

    cout << "Good bye." << endl;

    return 0;


}

int checkSuffix(char* userInput, int (*fn)(char*),
                const char* suffixName, const int suffixIndex)
{
    if(strLength(userInput) < 1)
        return NOTFOUND;

    const int curSuffixIndex = (*fn)(userInput);
    if(curSuffixIndex != NOTFOUND)
    {
        cout << suffixName << " suffix found: "
             << suffixes[suffixIndex][curSuffixIndex] << endl;

        #ifndef NDEBUG
                printIndex(curSuffixIndex,userInput);
        #endif

        return curSuffixIndex;
    }
    return NOTFOUND;
}

int checkRoot(char* userInput)
{
    if(strLength(userInput) < 1)
        return NOTFOUND;

    const int curKnownRootsIndex = findRoot(userInput);
    if(curKnownRootsIndex != NOTFOUND)
    {
        cout << "Root found: "
             << knownRoots[curKnownRootsIndex] << endl;

        #ifndef NDEBUG
                printIndex(curKnownRootsIndex,userInput);
        #endif

        return curKnownRootsIndex;
    }
    return NOTFOUND;
}

void getWord(char* word)
{
    cout << "Enter a string => ";
    cin >> word;
}

bool searchStr(char* root,const char* suffix)
{
    int index = searchStrInStr(suffix,root);
    if(index != NOTFOUND)
    {
        strCutEnd(root,index+strLength(suffix));

        #ifndef NDEBUG
            cout << "Suffix: " << suffix << "  Root: " << root << endl;
        #endif

        return true;
    }
    return false;
}

int searchInArr(char* root,const char sourceStr[][10],const int sourceSize)
{
    int index = NOTFOUND, asterixIndex;

    for(int i=0;i<sourceSize;++i)
    {
        // if suffix contains the char '*', it will be replaced by
        // asterixLetters and the different permutation of suffix will be tried.
        asterixIndex = findChar('*',sourceStr[i],1);
        //cout << "debug: " << asterixIndex << sourceStr[i] << endl;
        if(asterixIndex != NOTFOUND)
        {
            char newSuffix[INPUTSTRSIZE];
            for(int j=0;j<NUMOFASTERIXES;++j)
            {
                //copying asterix containing suffix to newSuffix
                strCpy(sourceStr[i],newSuffix);
                //replacing asterixes by asterixLetters' chars
                strReplace(newSuffix,'*',asterixLetters[j]);
                if(searchStr(root,newSuffix))
                    return i;
            }
            // trying root to find suffix without asterix
            strCpy(sourceStr[i],newSuffix);
            //removing asterix
            removeChar('*',newSuffix);
            if(searchStr(root,newSuffix))
                    return i;
        }
        if(searchStr(root,sourceStr[i]))
            return i;
    }
    return index;
}

int findSuffixes(char* root,int suffixIndex)
{
    return searchInArr(root,suffixes[suffixIndex],NUMOFSUFFIXES);
}

int findRoot(char* root)
{
    // assuming root as a suffix :)
    return searchInArr(root,knownRoots,NUMOFKNOWNROOTS);
}

// ##################### GENERAL SUFFIX FINDER FUNCTIONS #################### //
inline int findPluralSuffix(char* root)
{
    return findSuffixes(root,PLURALSUFFIXINDEX);
}

inline int findOwnershipSuffix(char* root)
{
    return findSuffixes(root,OWNERSUFFIXINDEX);
}

inline int findObjectSuffix(char* root)
{
    return findSuffixes(root,OBJECTSUFFIXINDEX);
}

inline int findStaticPosSuffix(char* root)
{
    return findSuffixes(root,STATICPOSSUFFIXINDEX);
}

inline int findMoveAwaySuffix(char* root)
{
    return findSuffixes(root,MOVEAWAYSUFFIXINDEX);
}

inline int findMoveToSuffix(char* root)
{
    return findSuffixes(root,MOVETOSUFFIXINDEX);
}

inline int findWithoutSuffix(char* root)
{
    return findSuffixes(root,WITHOUTSUFFIXINDEX);
}