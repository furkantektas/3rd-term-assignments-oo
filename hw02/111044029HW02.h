/*
 * File:    111044029HW01.h
 *
 * Course:  CSE241
 * Project: HW02
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on October 15, 2012, 7:18 AM
 */

#ifndef HEADER
#define	HEADER

#include <iostream>

#include "111044029HW02_Strings.h"

#define STRSIZE 10

#define NUMOFSUFFIXGROUP 7
#define NUMOFSUFFIXES 4
#define NUMOFASTERIXES 3
#define NUMOFKNOWNROOTS 15

#define WITHOUTSUFFIXINDEX 0
#define PLURALSUFFIXINDEX 1
#define MOVEAWAYSUFFIXINDEX 2
#define STATICPOSSUFFIXINDEX 3
#define OWNERSUFFIXINDEX 4
#define OBJECTSUFFIXINDEX 5
#define MOVETOSUFFIXINDEX 6

using namespace std;

void getWord(char* word);
int checkRoot(char* userInput);
int checkSuffix(char* userInput, int (*fn)(char*),
                const char* suffixName, const int suffixIndex);
// note that order of suffixes[] is important
const char suffixes[NUMOFSUFFIXGROUP][NUMOFSUFFIXES][STRSIZE] = {
                                    {"siz","suz"}, //Without
                                    {"ler","lar"}, //Plural
                                    {"den","dan","ten","tan"}, //Movement Away
                                    {"de","da","te","ta"}, //Static Position
                                    {"*m","*n"}, //Ownership
                                    {"*i","*u"}, //Object
                                    {"*e","*a"}, //Movement Towards
                                    };

// in turkish: kaynastirma harfleri.
const char asterixLetters[NUMOFASTERIXES] = {'n','s','y'};

// known roots for hw02
const char knownRoots[NUMOFKNOWNROOTS][STRSIZE] = {"abone","ada","adres",
                    "araba","afis","cumba","macera","mahmuz","seciye","seher",
                    "sentetik","seyyah","ic","dis","sihhi"};

#ifndef NDEBUG
    //debugging function
    void printIndex(int ind,const char str[])
    {
        cout << " Index :"<< ind << " Word: " << str << endl;
    }
#endif

// searches suffix in root
bool searchStr(char* root,const char* suffix);

// searches string in sourceStr array
// used for discriminating suffixes and words
int searchInArr(char* root,const char sourceStr[][10],const int sourceSize);

// find suffixes using searchInArr function
int findSuffixes(char* root,int suffixIndex);

// find root using searchInArr function
int findRoot(char* root);

// ##################### GENERAL SUFFIX FINDER FUNCTIONS #################### //
inline int findPluralSuffix(char* root);
inline int findOwnershipSuffix(char* root);
inline int findObjectSuffix(char* root);
inline int findStaticPosSuffix(char* root);
inline int findMoveAwaySuffix(char* root);
inline int findMoveToSuffix(char* root);
inline int findWithoutSuffix(char* root);

#endif	/* HEADER */