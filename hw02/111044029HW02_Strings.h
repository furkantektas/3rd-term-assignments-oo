/*
 * File:    111044029HW01_Strings.h
 *
 * Course:  CSE241
 * Project: HW02
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on October 15, 2012, 7:18 AM
 */

#ifndef STRINGS
#define	STRINGS

#define INPUTSTRSIZE 50
#define NOTFOUND -1

//########################  FUNCTION IMPLEMENTATIONS  ########################//

int strLength(const char ch[])
{
    int i=0;
    for(;ch[i]!='\0';i++)
        if(ch[i] == '\0')
            break;
    return i;
}

void strCpy(const char source[], char target[])
{
    int i;
    for(i=0;source[i] != '\0'; ++i)
        target[i] = source[i];
    target[i] = '\0';
}

void strPop(char str[])
{
    if(str[0] == '\0')
        return;
    short int index = strLength(str)-1;
    str[index] = '\0';
}

// cuts the string's last 'limit' chars
void strCutBegin(char* str, int limit)
{
    str[limit] = '\0';
}

// cuts the string's first 'limit' chars
void strCutEnd(char* str, int limit)
{
    strCpy(&str[limit],str);
}

void swapChar(char *ch1, char *ch2)
{
    char tempCh;
    tempCh = *ch1;
    *ch1 = *ch2;
    *ch2 = tempCh;
}

bool strEqual(const char str1[], const char str2[], int limitIndex)
{
    const int str1Len = strLength(str1),
              str2Len = strLength(str2);
    int maxStrLen;

    // maxStrLen equals the limitIndex if limitIndex is not 0
    if(limitIndex != 0)
        maxStrLen = limitIndex;
    // maxStrlen equals the bigger of str1Len and str2Len
    else
        maxStrLen = ((str1Len > str2Len) ? str1Len : str2Len);

    //checking the letters of str1 and str2 at first maxStrLen elements.
    for(int i=0;i<maxStrLen;++i)
        if(str1[i] != str2[i])
            return false;
    return true;
}


int findChar(const char ch, const char arr[], const int arrSize)
{
    for(int i=0;i<arrSize;++i)
        if(arr[i] == ch)
            return i;
    return NOTFOUND;
}


// removes a char from string
void removeChar(const char ch, char* str)
{
    const int index = findChar(ch,str,strLength(str));
    strCpy(&str[index+1],&str[index]);
}


// needs to be tested
void fillCharArray(char arr[], const int arrSize, const char ch = '\0')
{
     for(int i=0;i<arrSize;++i)
        arr[i] = ch;
     // putting null char at the end of the arr
     arr[arrSize-1] = '\0';
}

bool getUniqueLetters(const char num[], char distinctLetters[],
                      int& curDistinctLettersIndex)
{
    for(int i=0;i<strLength(num);++i)
    {
        if(findChar(num[i],distinctLetters,strLength(num)+1) == NOTFOUND)
        {
            // checking whether the current index is smaller than zero or not
            distinctLetters[curDistinctLettersIndex] = num[i];
            ++curDistinctLettersIndex;
        }
    }
    return true;
}

bool isDigit(const int num)
{
    if(num >= 0 && num <= 9)
        return true;
    return false;
}

void strReplace(char str[], const char oldValue, const char newValue)
{
    const int strLen = strLength(str);
    for(int i=0;i<strLen;++i)
        if(str[i] == oldValue)
            str[i] = newValue;
}

void pushStrToArr(const char str[], char* arr[], int arrIndex)
{
    strCpy(str,arr[arrIndex]);
    ++arrIndex;
}

// searches target in source and returns the first occurrence of the target in
// the source
int searchStrInStr(const char* target, const char* source)
{
    const int sourceLen = strLength(source),
              searchStrLen = strLength(target);
    int index;

    index = findChar(target[0],source,sourceLen);
    if(index != NOTFOUND)
        if(strEqual(target,&source[index],searchStrLen))
            return index;
    return NOTFOUND;
}

bool searchStrInArr(const char str[],const char* arr[],const int arrSize)
{
    for(int i=0;i<arrSize;++i)
        if(strEqual(str,arr[i],strLength(str)))
            return true;
    return false;
}

void strAppend(char* str, const char* appendText)
{
    strCpy(appendText,&str[strLength(str)-1]);
}

inline int charToDigit(const char ch)
{
    return static_cast<int>(ch) - 48; //48 is because ascii
}
//returns the integer value of the given char

#endif	/* STRINGS */

