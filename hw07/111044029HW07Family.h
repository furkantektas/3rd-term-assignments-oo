/*
 * File:    111044029HW07Family.h
 *
 * Course:  CSE241
 * Project: HW07
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef FAMILY_H
#define	FAMILY_H

#ifndef PERSONVECTOR_H
#include "111044029HW07PersonVector.h"
#endif

#include <fstream>
 
#define NOTFOUND -1

namespace Tektas{
	class Family {
		public:
			//constructor and copy constructor
			Family(String momName = "",
				   String dadName = "",
				   Family* bride = NULL, 
				   Family* groom = NULL);
			// Family(const Family& other);
			Family(const Person& mother, const Person& father);
            
			~Family();

			Family& operator+=(const Person& newBornBaby);
			// adds a new born baby to family

			Family operator+(Family& other);
			// takes another family and creates a new family with single and 
			// opposite sex childrens of these families.
			// const keyword should not used to change maritial status of 
			// bride and groom

			bool operator==(const Family& other);
			// compares two families. Returns true if the families are equal
			
			bool operator!=(const Family& other);
			// compares two families. Returns true if the families are not equal

			Person& operator[](unsigned int ind);
			const Person& operator[](unsigned int ind) const;
			// returns the child at given index
			
			friend std::ostream& operator<<(std::ostream& stream, 
				                            const Family& fam);
			friend std::istream& operator>>(std::istream& stream, Family& fam);

			// non const objects for the possibility of modifying
			Person& getMom();
			const Person& getMom() const;
			Person& getDad();
			const Person& getDad() const;
			Person& getChild(unsigned int ind);
			const Person& getChild(unsigned int ind) const;
			const Family& getBridesFamily() const;
			const Family& getGroomsFamily() const;
			
			const String getMomName() const;
			const String getDadName() const;
			const String getChildName(int numOfChild) const;
			int getNumOfPerson() const;
			int getNumOfChild() const;

			static int familyCounter; // num of living families 
			static int getNumOfLivingFamilies();
			friend std::fstream& writeFamilyToBinary(std::fstream& binFile, Family& fam);
			std::fstream& readFamilyFromBinary(std::fstream& binFile);
		private:
			PersonVector members; //family members
			Family* parentsOfGroom; // pointer for groom's family
			Family* parentsOfBride; // pointer for bride's family

			int validateIndex(int ind) const;
	};
}

#endif