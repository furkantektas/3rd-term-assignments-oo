/*
 * File:    111044029HW07Family.cpp
 *
 * Course:  CSE241
 * Project: HW07
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef FAMILY_H
#include "111044029HW07Family.h"
#endif

#include <iostream> //for cout
#include <cstdlib> //for exit

namespace Tektas{

	namespace {
		int getFirstSingle(const Family& fam, int index=0) {
			for(int i=index; i<=fam.getNumOfChild()-1; ++i)
				if(fam[i].isSingle())
					return i;
			return NOTFOUND;
		}

		int getFirstSingleMale(const Family& fam, int index=0) {
			if(index > (fam.getNumOfChild()-1))
				return NOTFOUND;
			
			int tempInd = getFirstSingle(fam,index);
			if(tempInd == NOTFOUND)
				return NOTFOUND;

			if(fam[tempInd].isMale())
				return tempInd;
			return getFirstSingleMale(fam,tempInd+1);
		}

		int getFirstSingleFemale(const Family& fam, int index=0) {
			if(index > (fam.getNumOfChild()-1))
				return NOTFOUND;
			
			int tempInd = getFirstSingle(fam,index);
			if(tempInd == NOTFOUND)
				return NOTFOUND;

			if(!(fam[tempInd].isMale()))
				return tempInd;
			return getFirstSingleFemale(fam,tempInd+1);
		}
	}
	//constructor and copy constructor
	Family::Family(String momName,
		           String dadName,
		           Family* groom, 
		           Family* bride) :
	               members(2),
	               parentsOfGroom(groom),
	               parentsOfBride(bride)
	       {
	       		if(!(momName.empty()) && !(dadName.empty())) {
		       		Person tempMom, tempDad;

	   		        tempDad.setFirstName(dadName.split(','));
			    	tempDad.setLastName(dadName.split(',',1));
			    	tempDad.setYearOfBirth(strToInt(dadName.split(',',2)));
			    	tempDad.setGender((dadName.split(',',3) ==  "male"));
			    	tempDad.setOwnFamily(this);
			    	tempDad.setParentsFamily(parentsOfGroom);

	   		        tempMom.setFirstName(momName.split(','));
			    	tempMom.setLastName(momName.split(',',1));
			    	tempMom.setYearOfBirth(strToInt(momName.split(',',2)));
			    	tempMom.setGender((momName.split(',',3) ==  "male"));
			    	tempMom.setOwnFamily(this);
			    	tempMom.setParentsFamily(parentsOfBride);

		       		members.push_back(tempMom);
		       		members.push_back(tempDad);
		       	}
		       	++Family::familyCounter;
	       };
	
	Family::Family(const Person& mother, const Person& father) :
		members(2), parentsOfGroom(father.getPFam()),
                parentsOfBride(mother.getPFam())
	{
		members.push_back(mother);
		members.push_back(father);
		getMom().setOwnFamily(this);
		getDad().setOwnFamily(this);
		++Family::familyCounter;
	}

	Family::~Family() {
		Family::familyCounter -= 1;
	}

	Family& Family::operator+=(const Person& newBornBaby) {
		members.push_back(newBornBaby);
		members[getNumOfPerson()-1].setParentsFamily(this);
	}

	Family Family::operator+(Family& other){
		if(this == &other) {
			std::cout << "Stop! You're siblings! :)\n";
			// http://www.youtube.com/watch?v=8mviu14Jr9Y
			exit(1);
		}
		int groom = getFirstSingleMale(*this);
		int bride = getFirstSingleFemale(other);
		Family temp;

		if((groom == NOTFOUND) || (bride == NOTFOUND)) {
			bride = getFirstSingleFemale(*this);
			groom = getFirstSingleMale(other);
			if((groom == NOTFOUND) || (bride == NOTFOUND)) {
				std::cout << "No appropriate child found. This marriage cannot be solemnized.\n";
				exit(1);
			}
			else {  // groom from other, bride from *this
				temp = Family(other[groom], operator[](bride));
				temp.getMom().setLastName(temp.getDad().getLastName());
				
				// setting groom and bride's maritial status as married
				// for both families
				temp.getMom().setMarried();
				temp.getDad().setMarried();
				other[groom].setMarried();
				operator[](bride).setMarried();
			}
		}
		else { // groom from *this, bride from other
			temp = Family(other[bride],operator[](groom));
			temp.getMom().setLastName(temp.getDad().getLastName());
			temp.getMom().setMarried();
			temp.getDad().setMarried();
			other[bride].setMarried();
			operator[](groom).setMarried();
		}
		return temp;
	}

	bool Family::operator==(const Family& other) {
		if(getNumOfPerson() != other.getNumOfPerson())
			return false;
		
		for(int i=0; i<getNumOfPerson(); ++i)
			if(members[i] != other.members[i])
				return false;

		return true;
	}
	
	bool Family::operator!=(const Family& other) {
		return !(*this == other);
	}

	Person& Family::operator[](unsigned int ind) {
		validateIndex(ind);
		return members[ind+2]; // 2 is for mom and dad
	}

	const Person& Family::operator[](unsigned int ind) const {	
		validateIndex(ind);
		return members[ind+2]; // 2 is for mom and dad
	}

	Person& Family::getMom() {
		return members[0];
	}
	
	const Person& Family::getMom() const{
		std::cout << members.size() << "\n";
		return members[0];
	}
	
	Person& Family::getDad() {
		return members[1];
	}
	
	const Person& Family::getDad() const {
		return members[1];
	}

	Person& Family::getChild(unsigned int ind) {
		return (*this)[ind];
	}

	const Person& Family::getChild(unsigned int ind) const{
		return (*this)[ind];
	}

	const Family& Family::getBridesFamily() const {
		return *parentsOfBride;
	}
	
	const Family& Family::getGroomsFamily() const {
		return *parentsOfGroom;
	}

	const String Family::getMomName() const {
		return members[0].getName();
	}

	const String Family::getDadName() const {
		return members[1].getName();	
	}

	const String Family::getChildName(int numOfChild) const {
		validateIndex(numOfChild+2);
		return members[numOfChild+2].getName();
	}
	
	int Family::getNumOfPerson() const {
		return members.size();
	}

	int Family::getNumOfChild() const {
		return getNumOfPerson() - 2;
	}

	int Family::validateIndex(int ind) const {
		if(ind > getNumOfPerson()-1) {
			std::cout << "Invalid index for family class. Terminating.\n";
			std::exit(1);
		}
		return ind;
	}

	std::ostream& operator<<(std::ostream& stream, const Family& fam) {
		stream << fam.members[1].getLastName() << " family:\n";
		for(int i=0;i<fam.members[1].getLastName().size()+9;++i)
			stream << "-";
		stream << "\nMom:\n" << fam.members[0] << std::endl;
		stream << "Dad:\n" << fam.members[1] << std::endl;
		for(int i=0; i<fam.getNumOfChild();++i)
			stream << "Child " << i+1 << ":\n" << fam[i] << std::endl;

		return stream;
	}

	std::istream& operator>>(std::istream& stream, Family& fam) {
		String momStr(80), dadStr(80);
		Person tempMom, tempDad;

		stream >> momStr;
        tempMom.setFirstName(momStr.split(','));
		tempMom.setLastName(momStr.split(',',1));
		tempMom.setYearOfBirth(strToInt(momStr.split(',',2)));
		tempMom.setGender((momStr.split(',',3) ==  "male"));

		stream >> dadStr;
        tempDad.setFirstName(dadStr.split(','));
		tempDad.setLastName(dadStr.split(',',1));
		tempDad.setYearOfBirth(strToInt(dadStr.split(',',2)));
		tempDad.setGender((dadStr.split(',',3) ==  "male"));

		fam = Family(tempMom, tempDad);
	}

	std::fstream& Family::readFamilyFromBinary(std::fstream& binFile) {
		members.readVectorFromBinary(binFile);
		std::cout << "End of family reading.\n";
	}

	std::fstream& writeFamilyToBinary(std::fstream& binFile, Family& fam) {
		writeVectorToBinary(binFile,fam.members);
		return binFile;
	}


	int Family::getNumOfLivingFamilies() {
		return Family::familyCounter;
	}

}
