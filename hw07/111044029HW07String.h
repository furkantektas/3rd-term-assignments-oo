/*
 * File:    111044029HW07String.h
 *
 * Course:  CSE241
 * Project: HW07
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef STRING_H
#define	STRING_H

#define NOTFOUND -1

#include <fstream> // for operator overloading

namespace Tektas{
    class String {

        //Simple String class for holding Strings.
        public:
            String() : strSize(0), strCapacity(0), str(NULL) {};
            explicit String(int capacityVal);
            String(const char* chArr);
            String(const String& other); //copy constructor
            ~String(); //destructor

            // appends charVal to str and places null character at the end.
            void push_back(const char charVal);
            
            // if strSize is bigger than 0, reduces the strSize by 1 and
            // replaces the last character with null character 
            void pop_back();

            int size() const;
            int capacity() const;

            char& at(unsigned int ind);
            const char& at(unsigned int ind) const;

            char& operator[](unsigned int ind);
            const char& operator[](unsigned int ind) const;

            // return false if name is empty
            bool empty();
            // sets strSize=0
            void clear(void);
            // reserves enough memory to hold str and a null character
            void reserve(int strLength);

            String split(const char delimiter, int occurence=0);
            // returns the string from occurence-th delimeter to (occurence+1)th delimeter

            int find(const char ch, int occurence=1);
            // returns the index of occurence-th occurence of ch
            // if ch is not found, return NOTFOUND

            String substr(int firstInd = 0, int lastInd = -1) const;
            // returns the string from firstInd to lastInd
            // use lastInd = -1 for end of the string

            // operator overloadings
            String& operator=(const String& other);
            String operator+(const String& other) const;
            bool operator==(const String& other) const;
            bool operator!=(const String& other) const;
            friend std::istream& operator>>(std::istream& stream, Tektas::String& strVal);
            friend std::ostream& operator<<(std::ostream& stream, const Tektas::String& strVal);

            void writeToBinary(std::fstream& binFile);
            void readFromBinary(std::fstream& binFile);
        private:
            char *str;
            int strSize;
            int strCapacity;
            void increaseCapacity();
            
            // exits if the ind is invalid
            void verifyIndex(int ind) const;


    };

    namespace{
        int strLen(const char* strVal) {
            unsigned int len = 0;
            while(strVal[len] != '\0')
                ++len;
            return len;
        }

        // returns the integer value of ch
        int charToInt(char ch) {
            if( (ch <= '9') && (ch >= '0'))
                return ch - '0';
            else
                return NOTFOUND;
        }

        // returns the integer value of given string
        int strToInt(const String& str, int curValue = 0) {
            
            curValue = curValue * 10 + charToInt(str.at(0));

            if(str.size() <= 1)
                return curValue;
            else
                return strToInt(str.substr(1), curValue);
        }        
    }
}
#endif	/* STRING_H */