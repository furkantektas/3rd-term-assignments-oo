/*
 * File:    111044029HW07String.h
 *
 * Course:  CSE241
 * Project: HW07
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef STRING_H
#include "111044029HW07String.h"
 #endif

#include <iostream> // for operator overloading
 #include <cstdlib> // for exit

namespace Tektas{
    
    String::String(int capacityVal): strSize(0), str(NULL) {
        reserve(capacityVal);
    }

    String::String(const char* chArr): strSize(0) {
        const int length = strLen(chArr);
        //reserving memory for holding chArr and null character
        reserve(length);

        // copying chArr to str
        for(unsigned int i=0; i<length; ++i)
           push_back(chArr[i]);
    }

    String::String(const String& other) : strSize(0), strCapacity(other.strSize+1) {
        reserve(strCapacity-1);
        
        for(int i=0; i<other.strSize; ++i)
            push_back(other.str[i]);
    }

    String::~String() {
        if(strSize > 0) 
            delete [] str;
    }

    void String::push_back(const char charVal) {
        if(strSize < (strCapacity-1))
            str[strSize] = charVal;
        ++strSize;
        str[strSize] = '\0';
    }

    void String::pop_back() {
        if(strSize > 0) {
            --strSize;
            str[strSize] = '\0';
        }
    }

    int String::size() const {
        return strSize;
    }

    int String::capacity() const {
        return strCapacity;
    }

    char& String::at(unsigned int ind) {
        verifyIndex(ind);
        return str[ind];
    }
    const char& String::at(unsigned int ind) const {
        verifyIndex(ind);
        return str[ind];
    }

    char& String::operator[](unsigned int ind) {
        return at(ind);
    }
    
    const char& String::operator[](unsigned int ind) const {
        return at(ind);
    }

    bool String::empty(void) {
        return (strSize == 0);
    }

    void String::clear(void) {
        strSize = 0;
    }

    void String::reserve(int strLength) {
        if(strSize == 0) {
            strCapacity = strLength+1; // +1 for null
            str = new char[strCapacity];
        }
        else {
            std::cout << "Cannot re-reserve memory for string.\n";
            std::exit(1);
        }
    }

    String String::split(const char delimiter, int occurence) {
        int beginning;
        
        if(occurence == 0)
            beginning = 0;
        else
            beginning = find(delimiter,occurence)+1;

        int finish = find(delimiter,occurence+1);

        if(finish == NOTFOUND)
            finish = strSize;

        String temp(finish-beginning);
        
        for(int i=beginning;i<finish;++i)
            temp.push_back(str[i]);

        return temp;    
    }

    int String::find(const char ch, int occurence) {
        int curOccurence = 0,
            i = 0;
        bool found = false;
        
        do
        {
            if(str[i] == ch) {
                ++curOccurence;
                if(occurence == curOccurence)
                    found = true;
            }
            ++i;
        } while ((i < strSize) && !found);
            

        if(found)
            return i-1;
        return NOTFOUND;

    }

    String String::substr(int firstInd, int lastInd ) const {
        if(lastInd == -1)
            lastInd = strSize-1;

        if(lastInd<firstInd) {
            std::cout << "Invalid indexes for substr. Terminating.\n";
            exit(1);
        }

        String tempStr(lastInd-firstInd+1);

        for(int i=firstInd; i<=lastInd; ++i)
            tempStr.push_back(str[i]);

        return tempStr;
    }

    // operator overloadings
    String& String::operator=(const String& other) {
        if(this != &other){
            clear();  // strSize = 0 for reserve and assingment
            if(strCapacity < other.strSize) {
                delete [] str;
                reserve(other.strSize);
            }
            for(int i=0; i < other.strSize; ++i)
                push_back(other.str[i]);
        }
        return *this;
    }
    
    String String::operator+(const String& other) const {
        String temp(strSize + other.strSize);
        for(int i=0; i<strSize; ++i)
            temp.push_back(str[i]);
        for(int i=0; i < other.strSize; ++i)
            temp.push_back(other.str[i]);
        return temp;
    }

    bool String::operator==(const String& other) const {
        if(strSize != other.strSize)
            return false;
        
        for(int i=0; i<strSize; ++i)
            if(str[i] != other.str[i])
                return false;
        
        return true;
    }

    bool String::operator!=(const String& other) const {
        return !(*this == other);
    }


    std::istream& operator>>(std::istream& stream, Tektas::String& strVal) {
        char tmpChArr[80];
        stream >> tmpChArr;
        String tempStr(tmpChArr);
        strVal = tempStr;
        return stream;
    }

    std::ostream& operator<<(std::ostream& stream, const Tektas::String& strVal) {
        stream << strVal.str;
        return stream;
    }

    void String::increaseCapacity() {
        String temp = *this;
        strCapacity *= 1.5;

        delete [] str;
        strSize = 0;
        reserve(strCapacity-1); //reserve() will increase capacity by 1
        
        *this = temp;
    }

    void String::verifyIndex(int ind) const {
        if(ind >= strSize) {
            std::cout << "Invalid String Index. Terminating..\n";
            std::exit(1);
        }
    }

    void String::writeToBinary(std::fstream& binFile) {
		// Writing string's size first
        binFile.write((char *)(&strSize),sizeof(strSize));

		// Then writing the string to the binary file
        binFile.write(str,sizeof(char)*strSize);
    }

    void String::readFromBinary(std::fstream& binFile) {
        int size;
        char *tmpChArr;

		// Reading the size of the string
        binFile.read((char *)&size,sizeof(size));

		// Allocating memory for string and a NULL character
        tmpChArr = new char[size+1];
    
		// Reading size*char from binary file
	    binFile.read(tmpChArr,(sizeof(char)*size));

		//placing NULL char at the end of tmpChArr
        tmpChArr[size] = '\0';

        *this = String(tmpChArr);
        delete [] tmpChArr;
    }

}
