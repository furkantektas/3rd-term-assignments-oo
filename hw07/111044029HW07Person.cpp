/*
 * File:    111044029HW07Person.h
 *
 * Course:  CSE241
 * Project: HW07
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef PERSON_H
#include "111044029HW07Person.h"
#endif

#ifndef STRING_H
#include "111044029HW07String.h"
#endif

#include <cstdlib> // for exit
#include <iostream> // for cout
namespace Tektas{
	Person::Person() : 
	               firstName(),
	               lastName(),
	               gender(true),
		           yearOfBirth(1970),
		           single(false),
		           _pFam(NULL),
                   _oFam(NULL)
		           {/* deliberately left blank */}

	Person::Person(String fName, String sName, bool sex,
                   int birthYear, bool married, 
                   Family *pFam, Family *oFam) :
		           firstName(fName), lastName(sName), gender(sex),
		           yearOfBirth(validateYearOfBirth(birthYear)),
		           single(married),
		           _pFam(pFam),
                   _oFam(oFam)
		           {/* deliberately left blank */}

	Person::Person(const Person& other) :
	               firstName(other.firstName),
	               lastName(other.lastName),
	               yearOfBirth(other.yearOfBirth),
	               gender(other.gender),
	               single(other.single),
	               _pFam(other._pFam),
	               _oFam(other._oFam)
	               {/* deliberately left blank */}

	Family& Person::getParentsFamily() const{
		if(_pFam == NULL) {
			std::cout << "Invalid Parent's Family Pointer. Terminating..\n";
			exit(1);
		}
		return *(_pFam);
	}

	Family& Person::getOwnFamily() const{
		if(_oFam == NULL) {
			std::cout << "Invalid Own Family Pointer. Terminating..\n";
			exit(1);
		}
		return *(_oFam);
	}
        
        Family* Person::getPFam() const{
                return _pFam;
	}

	const String Person::getFirstName() const {
		return firstName;
	}

	const String Person::getLastName() const {
		return lastName;
	}

	const String Person::getName() const {
		return (firstName + " " + lastName);
	}

    int Person::getYearOfBirth() const {
    	return yearOfBirth;
    }

    //  CURYEAR is defined in header file.
	int Person::getAge() const {
		return (CURYEAR - yearOfBirth);
	}

    bool Person::isMale() const {
    	return gender;
    }
	
	bool Person::isSingle() const {
		return single;
	}

	void Person::setParentsFamily(Family* fam) {
		_pFam = fam;
	}

	void Person::setOwnFamily(Family* fam) {
		_oFam = fam;
	}

	void Person::setFirstName(const String& fName) {
		firstName = fName;
	}

	void Person::setLastName(const String& lName) {
		lastName = lName;
	}

	void Person::setYearOfBirth(int birthYear) {
		yearOfBirth = validateYearOfBirth(birthYear);
	}

	void Person::setGender(bool sex) {
		gender = sex;
	}

	void Person::setMarried() {
		single = false;
	}

	Person& Person::operator=(const Person& other) {
		if(this != &other)
		{
			firstName = other.firstName;
			lastName = other.lastName;
			gender = other.gender;
			yearOfBirth = other.yearOfBirth;
			single = other.single;
			_pFam = other._pFam;
			_oFam = other._oFam;
		}
		return *this;
	}

	bool Person::operator==(const Person& other) const {
		return ((firstName == other.firstName) && (lastName == other.lastName)
		        && (yearOfBirth == other.yearOfBirth) && (gender == other.gender)
		        && (single == other.single) && (_pFam == other._pFam)
                && (_oFam == other._oFam));
	}

	bool Person::operator!=(const Person& other) const {
		return !(*this == other);
	}
    std::ostream& operator<<(std::ostream& stream, const Person& obj) {
        stream << "Name            : " << obj.getName() << std::endl
               << "Year of birth   : " << obj.getYearOfBirth() << std::endl
               << "Marital Status  : ";
        
        if(obj.single)
        	stream << "Single\n";
        else
        	stream << "Married\n";
        	
        stream << "Gender          : ";

        if(obj.gender)
        	stream << "Male";
        else
        	stream << "Female";
        
    	stream << std::endl;
        return stream;
    }

    std::istream& operator>>(std::istream& stream, Person& obj) {
    	String input(80);
    	stream >> input;
    	obj.setFirstName(input.split(','));
    	obj.setLastName(input.split(',',1));
    	obj.setYearOfBirth(strToInt(input.split(',',2)));
    	obj.setGender((input.split(',',3) ==  "male"));
    	obj._pFam = NULL;
    	obj._oFam = NULL;
    }

    void Person::readPersonFromBinary(std::fstream& binFile) {
		// Using string class' readFromBianry() function to read
		// first and last names
    	firstName.readFromBinary(binFile);
    	lastName.readFromBinary(binFile);

		// Then reading the yearOfBirth, gender, and marital status
    	binFile.read((char *)&yearOfBirth,sizeof(yearOfBirth));
    	binFile.read((char *)&gender,sizeof(gender));
    	binFile.read((char *)&single,sizeof(single));
    }

    std::fstream& writePersonToBinary(std::fstream& binFile,
                                       Person& person) {
    	// Writing first and last names by calling string class'
		// writeToBinary() function
    	person.firstName.writeToBinary(binFile);
    	person.lastName.writeToBinary(binFile);
    	
		// Then writing the yearOfBirth, gender, and marital status
    	binFile.write((char *)&person.yearOfBirth,sizeof(person.yearOfBirth));
    	binFile.write((char *)&person.gender,sizeof(person.gender));
    	binFile.write((char *)&person.single,sizeof(person.single));
    }

}
