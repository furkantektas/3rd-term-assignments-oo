/*
 * File:    111044029HW07.h
 *
 * Course:  CSE241
 * Project: HW07
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef FAMILY_H
	#include "111044029HW07Family.h"
#endif

#include <fstream>

using namespace Tektas;
using std::fstream;
using std::cout;
using std::endl;
using std::boolalpha;
using std::ios;


int Tektas::Family::familyCounter = 0;

// generate families for writing to binary
void generateAndSaveFamilies();

// writing families to binary
void writeToBinary(Family** fams, int numOfFams, fstream& binFile);

// reading families from binary
void readFamilies();

// searches p2 in the family and ancestor's families of f1
bool isRelative(const Person& p1, const Person& p2,
    	        Family** fams, int numOfFams);


void displayFamilyTree(Person& obj){
    using std::cout;
    if(obj.getPFam() == NULL) {
        cout << "Liseli detected!\n";
        return;
    }
    Family p1Fam= *(obj.getPFam());

    cout << p1Fam << "\n";
    displayFamilyTree(p1Fam.getDad());
}

int main(int argc, char const *argv[])
{
	generateAndSaveFamilies();
	std::cout << "Families are wrote to binary file.\n";
	readFamilies();
	return 0;
}

void generateAndSaveFamilies() {

	// Nicholas I's family
	Person pNicholas1("Nicholas I","Romanov",true,1796,false),
	       n1Wife("Alexandra","Feodorovna",false,1798,false);

	Family fNicholas1(n1Wife,pNicholas1);
	
	// Adding children of Nicholas I
	fNicholas1 += Person("Elizabeth","Romanov",false,1826,true);// not married
	fNicholas1 += Person("Maria","Romanov",false,1819,false); 
	fNicholas1 += Person("Alexandra","Romanov",false,1825,false); 
	fNicholas1 += Person("Constantine","Romanov",true,1827,false); 
	fNicholas1 += Person("Alexander II","Romanov",true,1818,false);// next emperor
	fNicholas1 += Person("Michael","Romanov",true,1832,false); 
	fNicholas1 += Person("Olga","Romanov",false,1822,false); 
	fNicholas1 += Person("Nicholas","Romanov",true,1831,false); 

	// Adding grandchildrens of Nicholas I
	// Maria
	Family(fNicholas1[1], Person("Maximilian de","Beauharnais",true,1839,true));
	// Alexandra
	Family(fNicholas1[1], Person("Maximilian de","Beauharnais",true,1839,true));
	// Maria
	Family(fNicholas1[1], Person("Maximilian de","Beauharnais",true,1839,true));

	
	// Alexander II's family
	Person pA2Wife("Maria","Alexandrovna",false,1824,false);
	
	Family fAlexander2(pA2Wife,fNicholas1[4]);

	// Adding children of Alexander II
	fAlexander2 += Person("Vladimir","Romanov",true,1847,false);
	fAlexander2 += Person("Alexandra","Romanov",false,1842,true);// not married
	fAlexander2 += Person("Nicholas","Romanov",true,1843,true);// not married
	fAlexander2 += Person("Sergei","Romanov",true,1857,false);
	fAlexander2 += Person("Alexander III","Romanov",true,1845,false);// next emperor
	fAlexander2 += Person("Alexei","Romanov",true,1850,false);
	fAlexander2 += Person("Paul","Romanov",true,1860,false);
	fAlexander2 += Person("Maria","Romanov",false,1853,false);

	// Alexander II's family
	Person a3Wife("Maria","Feodorovna",false,1847,false);

	// cout << fAlexander2[4] << endl;
	Family fAlexander3(a3Wife,fAlexander2[4]);

	// Adding children of Alexander II
	fAlexander3 += Person("Alexander","Romanov",true,1869,true);// not married
	fAlexander3 += Person("George","Romanov",true,1870,true);// not married
	fAlexander3 += Person("Xenia","Romanov",false,1875,false);
	fAlexander3 += Person("Nicholas II","Romanov",true,1868,false);// next emperor
	fAlexander3 += Person("Olga","Romanov",false,1882,false);
	fAlexander3 += Person("Michael","Romanov",true,1878,false);


	// Nicholas III's family
	Person n3Wife("Alexandra","Fyodorovna",false,1872,false);

	Family fNicholas3(n3Wife, fAlexander3[3]);

	// Adding children of Nicholas III
	fNicholas3 += Person("Olga","Romanov",false,1895,false);// not married
	fNicholas3 += Person("Tatiana","Romanov",false,1897,false);// not married
	fNicholas3 += Person("Maria","Romanov",false,1899,false);// not married
	fNicholas3 += Person("Anastasia","Romanov",false,1901,false);// not married
	fNicholas3 += Person("Alexei","Romanov",true,1904,false);// not married

	Family *fams[] = {
		&fNicholas1,
		&fAlexander2,
		&fAlexander3,
		&fNicholas3,
	};


	fstream binFile("111044029.dat", ios::out | ios::binary);
	writeToBinary(fams,4,binFile);
	binFile.close();

	// testing whether the nicholas II and and alexander II is relative
//	cout << "Are Nicholas II and Alexander II relative? => " 
//	     <<  boolalpha <<  isRelative(fAlexander3[3],fNicholas3[4],fams, 4) << endl;
//        cout << boolalpha <<  isRelative(fNicholas1[3],fNicholas1[2],fams, 4);
    displayFamilyTree(fNicholas1[3]);
}

void writeToBinary(Family** fams, int numOfFams, fstream& binFile) {
	binFile.write((char *)&numOfFams, sizeof(numOfFams));
	for(int i=0; i< numOfFams; ++i)
		writeFamilyToBinary(binFile, **(fams+i));
}

void readFamilies() {
	fstream binFile("111044029.dat", ios::in | ios::binary);
	int familyNum;

	// reading the num of family
	binFile.read((char *)&familyNum, sizeof(familyNum));
	Family *families = new Family[familyNum];
	
	// reading families from binary
	for(int i=0; i<familyNum && families[i].readFamilyFromBinary(binFile); ++i);

	binFile.close();

	// printing the families
	for(int i=0; i<familyNum; ++i)
		std::cout << families[i] << std::endl << std::endl;
}

bool isRelative(const Person& p1, const Person& p2,
	            Family** fams, int numOfFams) {
    
        
        if(&p1 == NULL || &p2 == NULL)
            return false;	
	
        bool temp = false;
	
	if(p1 == p2) 
        return true; // same person

        Family p1Fam= (p1.getPFam() != NULL)? *(p1.getPFam()) : p1.getOwnFamily(),
               p2Fam= (p2.getPFam() != NULL)? *(p2.getPFam()) : p2.getOwnFamily();
        
        if(!(temp = isRelative(p1Fam.getDad(),p2Fam.getDad(), fams, numOfFams)))
			if(!(temp = isRelative(p1Fam.getDad(),p2Fam.getMom(), fams, numOfFams)))
				if(!(temp = isRelative(p1Fam.getMom(),p2Fam.getDad(), fams, numOfFams)))
					if(!(temp = isRelative(p1Fam.getMom(),p2Fam.getMom(), fams, numOfFams)))
						return false;
	return true;
}