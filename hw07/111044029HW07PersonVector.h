/*
 * File:    111044029HW07PersonVector.h
 *
 * Course:  CSE241
 * Project: HW07
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef PERSONVECTOR_H
#define	PERSONVECTOR_H

#ifndef PERSON_H
#include "111044029HW07Person.h"
#endif

 #include <fstream>

namespace Tektas{
	class PersonVector {
		public:
			PersonVector();
			PersonVector(int capacity);
			PersonVector(const PersonVector& other); //copy constructor
			~PersonVector(); //destructor

			void push_back(const Person& person);
			void pop_back();

			int size() const;
			int capacity() const;

			Person& at(unsigned int ind);
			const Person& at(unsigned int ind) const;

			Person& operator[](unsigned int ind);
			const Person& operator[](unsigned int ind) const;

			bool operator==(const PersonVector& other) const;
			bool operator!=(const PersonVector& other) const;

			PersonVector& operator=(const PersonVector& other);

			bool empty(void);
			void clear(void);
			void remove(void);
			void reserve(int capacity);
			friend std::fstream& writeVectorToBinary(
				std::fstream& binFile, PersonVector& vec);
			void readVectorFromBinary(std::fstream& binFile);
		private:
			int vectorSize;
			int vectorCapacity;
			Person *vector;

			void increaseCapacity();
			void validateIndex(int ind) const;
	};
}

#endif