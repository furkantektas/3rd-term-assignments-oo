/*
 * File:    111044029HW07Person.h
 *
 * Course:  CSE241
 * Project: HW07
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef PERSON_H
#define	PERSON_H

#ifndef STRING_H
#include "111044029HW07String.h"
#endif

 #include <iostream>
 #include <fstream>

namespace Tektas{
    
    unsigned const int CURYEAR = 2012;

    class Family; // forward reference for forward Family reference
    class Person {
        public:
            // constructors
            Person();
            Person(String fName, String sName, bool sex,
                   int birthYear, bool married = false, 
                   Family *pFam = NULL, Family *oFam = NULL);
            Person(const Person& other);

            //getters
            Family& getParentsFamily() const; // should not be const to possible
            Family* getPFam() const;
            // change of person's variables
            Family& getOwnFamily() const;
            const String getFirstName() const;
            const String getLastName() const;
            const String getName() const; // first name + last name
            int getYearOfBirth() const;
            int getAge() const;
            bool isMale() const; // true for male, false for female
            bool isSingle() const;

            //setters
            void setParentsFamily(Family* fam);
            void setOwnFamily(Family* fam);
            void setFirstName(const String& fName);
            void setLastName(const String& fName);
            void setYearOfBirth(int birthYear);
            void setGender(bool gender);
            void setMarried();

            // operator overloadings
            Person& operator=(const Person& other);
            bool operator==(const Person& other) const;
            bool operator!=(const Person& other) const;

            // Person >> sample input: furkan,tektas,1991,male
            friend std::istream& operator>>(std::istream& stream, Person& obj);
            friend std::ostream& operator<<(std::ostream& stream, const Person& obj);
            friend std::fstream& writePersonToBinary(std::fstream& binFile,
                                                     Person& person);
            void readPersonFromBinary(std::fstream& binFile);
        private:
            Tektas::String firstName, lastName; // note that its not the std::String
            short int yearOfBirth; // 1900..2012
            bool gender; // true for male false for female
            bool single; // true for single false for married
            Family* _pFam; // pointer of parent's family
            Family* _oFam; // pointer of own family
    };
    namespace{
        int validateYearOfBirth(int year) {
            if ((year > 1700) && (year < CURYEAR))
                return year;
            return 1700;
        }
    }
}

#endif	/* PERSON_H */