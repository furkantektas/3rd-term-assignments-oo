/*
 * File:    111044029HW07Main-Read.cpp
 *
 * Course:  CSE241
 * Project: HW07
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef FAMILY_H
	#include "111044029HW07Family.h"
#endif

#include <fstream>

using namespace Tektas;
using std::fstream;
using std::cout;
using std::endl;
using std::ios;


int Tektas::Family::familyCounter = 0;

// Reading families from binary
void readFamilies();

void displayFamilyTree(Person& obj){
    using std::cout;
    if(obj.getPFam() == NULL)
        return;
    
    Family p1Fam= *(obj.getPFam());

    cout << p1Fam << "\n";
    displayFamilyTree(p1Fam.getDad());
}

int main(int argc, char const *argv[])
{
	readFamilies();
	return 0;
}
void readFamilies() {
	fstream binFile("111044029.dat", ios::in | ios::binary);
	int familyNum;

	// Reading the num of family
	binFile.read((char *)&familyNum, sizeof(familyNum));
	Family *families = new Family[familyNum];
	
	// Reading families from binary
	for(int i=0; i<familyNum && families[i].readFamilyFromBinary(binFile); ++i);

	binFile.close();

	// Printing the families
	for(int i=0; i<familyNum; ++i)
		std::cout << families[i] << std::endl << std::endl;
}
