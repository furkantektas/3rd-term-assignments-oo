/*
 * File:    CSE241_HW01_111044029.cpp
 *
 * Course:  CSE241
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on October 2, 2012, 7:56 AM
 */


#include "111044029HW01.h"

int main(int argc, char* argv[]) {
    if(argc > 1 && argc != 4)
    {
        printUsage(argv[0]);
        return 0;
    }

   char inputs[NUMOFINPUT][INPUTSTRSIZE];

    //initializing the inputs
    for(int i=0; i<NUMOFINPUT;++i)
        fillCharArray(inputs[i],INPUTSTRSIZE,'\0');

    //using the console line parameters as inputs
    if(argc == NUMOFINPUT+1) {
        cout << "Inputs Are: ";
        for(int i=1;i<=NUMOFINPUT;++i)
        {
            cout << argv[i] << " ";
            strCpy(argv[i],inputs[i-1]);
        }
        cout << endl;
    }
    // getting inputs from user
    else
        for(int i=0; i<NUMOFINPUT;++i)
                getNum(inputs[i], i+1);

    //validating user inputs before processing the strings
    if(!validateUserInput(inputs))
        return 12;

    char distinctLetters[INPUTSTRSIZE];
    // contains all of the characters of num1,num2 and sum excepts digits
    // without duplicate chars

    int curDistinctLettersIndex = 0;

    fillCharArray(distinctLetters,INPUTSTRSIZE,'\0');
    //fills distinctLetters with NULL char

    //fill distinctLetters with inputs' distinct Letters
    for(int i=0; i<NUMOFINPUT;++i)
        if(!getUniqueLetters(inputs[i],distinctLetters,curDistinctLettersIndex))
            return 13;

    #ifndef NDEBUG
            cout << "Distinct Letters => " << distinctLetters << endl
                 << "curDistinctLetters: " << curDistinctLettersIndex << endl;
    #endif

    char nums[INPUTSTRSIZE];
    // nums arr contains the numbers for brute-force solving


    //filling nums with digits
    for(int i=0;i<INPUTSTRSIZE;++i)
        nums[i] = i+48;
    nums[INPUTSTRSIZE-1] = '\0';

    cout << "Calculating...\n";

    // calculating the possible solutions
    solve(nums, 0, strLength(nums)-1, distinctLetters, inputs);

    // Informing the user if no results found
    if(curSolutionIndex < 1)
        cout << "No solution found. Terminating. \n";

    return 0;
}

//########################  FUNCTION IMPLEMENTATIONS  ########################//

//######################  ALGORTIHM SPESIFIC FUNCTIONS  ######################//

void getNum(char num[], const int label)
{
    cout << "Enter the number " << label << ": ";
    cin  >> num;
}

bool validateUserInput(const char inputs[NUMOFINPUT][INPUTSTRSIZE])
{
    const int n1Length = strLength(inputs[0]),
              n2Length = strLength(inputs[1]),
              sumLength = strLength(inputs[2]),
              maxDigitNumber = (n1Length > n2Length) ? n1Length : n2Length;

#ifndef NDEBUG
    cout << "Number Lengths: " << endl
         << n1Length << endl
         << n2Length << endl
         << sumLength << endl;
#endif

    // sum cannot be smaller than  num1 or num2
    if(sumLength < maxDigitNumber) {
        cout << "Error: sum is too small. Terminating. \n";
        return false;
    }
    // digit number of sum cannot be 3+ digit more than
    // bigger num.
    else if(sumLength > (maxDigitNumber + 2) )
    {
        cout << "Error: sum is too big. Terminating. \n";
        return false;
    }

    // checking whether the inputs contains a digit or not
    for(int j=0;j<NUMOFINPUT;++j)
    {
        for(int i=0;i<=n1Length;++i)
        {
            if(isDigit(charToDigit(inputs[0][i])))
            {
                cout << "Number" << j << "contains digit: " << inputs[j][i]
                     << "'. Terminating\n";
                return false;
            }
        }
    }
    return true;
}


bool evaluate(char inputs[NUMOFINPUT][INPUTSTRSIZE], int carry)
{
    bool result;
    short int newSum, digit1, digit2, digitSum;
    const short int n1Index  = strLength(inputs[0])-1,
                    n2Index  = strLength(inputs[1])-1,
                    sumIndex = strLength(inputs[2])-1,
                    maxDigitNum = 1 + ((n1Index > n2Index) ? n1Index : n2Index);

    // converting the digits at the current indexes int
    // if inputs' index is zero assume it as zero
    digit1   = (n1Index < 0)  ? 0 : charToDigit(inputs[0][n1Index]);
    digit2   = (n2Index < 0)  ? 0 : charToDigit(inputs[1][n2Index]);
    digitSum = charToDigit(inputs[2][sumIndex]);

    // calculating sum with using carry
    newSum = digit1 + digit2 + carry;

    // verifying the result
    result = (digitSum == (newSum % 10));

    // updating the new carry according to newSum
    carry = (newSum > 9) ? 1 : 0;

    // if the current digits are the last digits of the first two input
    // return the result
    if(maxDigitNum == 1)
    {
        if(result && sumIndex == 0)
            result = result;
        else if(result && sumIndex == 1 && carry == 1 && (inputs[2][0]) == '1')
            result = true;
        else
            result = false;
        return result;
    }

    // if there is still digits to calculate, pop the inputs' last elements
    for(int i=0;i<NUMOFINPUT;++i)
        strPop(inputs[i]);

    // return the result.
    return result && evaluate(inputs, carry); //short circuit
}

bool verify(char nums[],char letters[], char inputs[NUMOFINPUT][INPUTSTRSIZE])
{
    bool verified = false;
    char inputsCp[NUMOFINPUT][INPUTSTRSIZE];

    const short int n1Length  = strLength(inputs[0]),
                    n2Length  = strLength(inputs[1]),
                    sumLength = strLength(inputs[2]),
                    minNumLength = (n1Length > n2Length) ? n2Length : n1Length,
                    maxNumLength = (n1Length < n2Length) ? n2Length : n1Length;

    // sum cannot be smaller than  num1 or num2
    if(sumLength < minNumLength)
        return false;
    else if(sumLength > maxNumLength+1)
        return false;

    // cloning the inputs
    for(int i=0; i<NUMOFINPUT;++i)
        strCpy(inputs[i],inputsCp[i]);

    // replacing the nums' elements with corresponding chars of nums to letters
    for(int i=0; i<strLength(letters);++i)
        for(int j=0;j<NUMOFINPUT;++j)
            strReplace(inputsCp[j],letters[i],nums[i]);

    // evaluate the correctness of replaced inputs
    if(evaluate(inputsCp))
        verified = true;
    return verified;
}

void solve(char nums[], int begin, int end, char letters[],
           char inputs[NUMOFINPUT][INPUTSTRSIZE])
{
    // permutation completed
    if (begin == end)
   {
        // verify the generated permutation
        if(verify(nums,letters,inputs)) {
            // permutation provides the correct answer
            char foundSolution[INPUTSTRSIZE];

            // copying current permutation to foundSolution
            strCpy(nums,foundSolution);

            // cutting the foundSolution at letters' length
            cutStr(foundSolution,strLength(letters));

            // searching for foundSolution in former solutions
            if(!searchInSolutions(foundSolution))
            {
                // no entries found in former solutions
                // add the current solution to solutions array
                addSolution(foundSolution);

                //printing the current solution
                cout << "Solution : " << "(";
                for(int i=0;letters[i] != '\0';++i)
                    cout << " " << letters[i] << ":" << foundSolution[i] << " ";
                cout << ")\n";
            }
       }
   }
   else
   {
       for (int i=begin; i<=end; ++i)
       {
            //generating new permutation
            swapChar(&nums[begin], &nums[i]);
            //trying to solve addition with generated nums permutation
            solve(nums, begin+1, end, letters, inputs);
            // swapping nums' chars to the former state
            swapChar(&nums[begin], &nums[i]);
       }
   }
}

void printUsage(char* programName)
{
    cout << "Usage:\n"
         << programName << " input1 input2 sum\n"
         << "Note: Parameters are case-sensitive\n";
}

//############################  STRING FUNCTIONS  ############################//

void swapChar(char *ch1, char *ch2)
{
    char tempCh;
    tempCh = *ch1;
    *ch1 = *ch2;
    *ch2 = tempCh;
}

bool equalStr(const char str1[], const char str2[], int limitIndex)
{
    const int str1Len = strLength(str1),
              str2Len = strLength(str2);
    int maxStrLen;

    // maxStrLen equals the limitIndex if limitIndex is not 0
    if(limitIndex != 0)
        maxStrLen = limitIndex;
    // maxStrlen equals the bigger of str1Len and str2Len
    else
        maxStrLen = ((str1Len > str2Len) ? str1Len : str2Len);

    //checking the letters of str1 and str2 at first maxStrLen elements.
    for(int i=0;i<maxStrLen;++i)
        if(str1[i] != str2[i])
            return false;
    return true;
}


int findChar(const char ch, char arr[], const int arrSize)
{
    for(int i=0;i<arrSize;++i)
        if(arr[i] == ch)
            return i;
    return -1;
}

// needs to be tested
void fillCharArray(char arr[], const int arrSize, const char ch)
{
     for(int i=0;i<arrSize;++i)
        arr[i] = ch;
     // putting null char at the end of the arr
     arr[arrSize-1] = '\0';
}

int strLength(const char ch[])
{
    int i=0;
    for(;ch[i]!='\0';i++)
        if(ch[i] == '\0')
            break;
    return i;
}

void strCpy(const char sourceStr[], char targetStr[])
{
    int i;
    for(i=0;sourceStr[i] != '\0'; ++i)
        targetStr[i] = sourceStr[i];
    targetStr[i] = '\0';
}

void strPop(char str[])
{
    if(str[0] == '\0')
        return;
    short int index = strLength(str)-1;
    str[index] = '\0';
}

bool getUniqueLetters(const char num[], char distinctLetters[],
                      int& curDistinctLettersIndex)
{
    for(int i=0;i<strLength(num);++i)
    {
        if(findChar(num[i],distinctLetters,strLength(num)+1) == -1)
        {
            // checking whether the current index is smaller than zero or not
            if(curDistinctLettersIndex < INPUTSTRSIZE)
            {
                distinctLetters[curDistinctLettersIndex] = num[i];
                ++curDistinctLettersIndex;
            }
            // more than 10 distinct letters found. Prompt and return.
            else
            {
                cout << "Error: There are more than 10 distinct letters.\n"
                     << "Terminating \n";
                return false;
            }
        }
    }
    return true;
}

bool isDigit(const int num)
{
    if(num >= 0 && num <= 9)
        return true;
    return false;
}

void strReplace(char str[], const char oldValue, const char newValue)
{
    const int strLen = strLength(str);
    for(int i=0;i<strLen;++i)
        if(str[i] == oldValue)
            str[i] = newValue;
}

void addSolution(const char str[])
{
    strCpy(str,solutions[curSolutionIndex]);
    ++curSolutionIndex;
}

bool searchInSolutions(const char str[])
{
    for(int i=0;i<curSolutionIndex;++i)
        if(equalStr(str,solutions[i],strLength(str)))
            return true;
    return false;
}