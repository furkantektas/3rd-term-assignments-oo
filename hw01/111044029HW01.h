/*
 * File:    111044029HW01.h
 *
 * Course:  CSE241
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on October 2, 2012, 7:56 AM
 */

#ifndef HEADER
#define	HEADER

#include <iostream>
using namespace std;

// comment to see debug info
#define	NDEBUG

//############################  GLOBAL VARIABLES #############################//

const int INPUTSTRSIZE = 11, //maximum user input
          NUMOFINPUT = 3, //num of user input
          MAXSOLUTIONSNUM = 1000; //maximum size of solutions array
// increase MAXSOLUTIONSNUM if program crashes with segmentation fault error

// current solutions array index
int curSolutionIndex = 0;

// solutions database
char solutions[MAXSOLUTIONSNUM][INPUTSTRSIZE];

//######################  ALGORTIHM SPESIFIC FUNCTIONS  ######################//

void getNum(char num[], const int label);
// prompts user and gets num from user and copies it to numCp

bool validateUserInput(const char inputs[NUMOFINPUT][INPUTSTRSIZE]);
// checks whether the user inputs' lengths are appropriate and inputs do not
// contain digits

bool evaluate(char inputs[NUMOFINPUT][INPUTSTRSIZE], int carry = 0);
// checks the correctness of the inputs' sum

bool verify(char nums[],char letters[], char inputs[NUMOFINPUT][INPUTSTRSIZE]);
// replaces the inputs' chars according to letters' corresponding nums elements
// verifies the sum of inputs by calling evaluate() function

void solve(char nums[], int begin, int end, char letters[],
           char inputs[NUMOFINPUT][INPUTSTRSIZE]);
// searches for appropriate numbers to fill inputs[] to get a correct addition
// generates different permutations with nums[] and verifies the permutations
// by calling verify() function

void printUsage(char* programName);
// prints the appropriate usage if user enters different number of command
// line parameters than 3


//############################  STRING FUNCTIONS  ############################//

void swapChar(char *ch1, char *ch2);
// swaps two chars.

bool equalStr(const char str1[], const char str2[], int limitIndex);
// returns whether the limitIndex characters of the str1 and str2 is the equal

int findChar(const char ch, char arr[], const int arrSize);
//finds the occuurence of ch in arr and returns its position.
// if arr does not contain ch, return -1

void fillCharArray(char arr[], const int arrSize, const char ch = '\0');
// fills arr in given size with ch

int strLength(const char ch[]);
// returns the number of chars before NULL char.

void strCpy(const char sourceStr[], char targetStr[]);
// copies the sourceStr to the targetrStr

void strPop(char str[]);
// pops the last element of str

bool getUniqueLetters(const char num[], char distinctLetters[],
                      int& curDistinctLettersIndex);
// Makes the letters of the nums' strings unique and
// sets the curDistinctLettersIndex
bool isDigit(const int num);
// returns whether the integer is a digit or not

void strReplace(char str[], const char oldValue,const char newValue);
// replaces char(s) in a given string

bool searchInSolutions(const char str[]);
// searches str in solutions[]

void addSolution(const char str[]);
// adds a new record to solutions[]

inline int charToDigit(const char ch)
{
    return static_cast<int>(ch) - 48; //48 is because ascii
}
//returns the integer value of the given char

inline void cutStr(char str[], int limit)
{
    str[limit] = '\0';
}
// cuts str at a given limit

#endif	/* MAIN_H */