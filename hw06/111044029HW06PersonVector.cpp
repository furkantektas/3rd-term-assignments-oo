/*
 * File:    111044029HW06PersonVector.cpp
 *
 * Course:  CSE241
 * Project: HW06
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */


#ifndef PERSONVECTOR_H
#include "111044029HW06PersonVector.h"
#endif

#include <iostream> // for cout
#include <cstdlib>  // for exit

namespace Tektas{
	PersonVector::PersonVector() :
	              vectorSize(0),
	              vectorCapacity(0),
	              vector(NULL)
	{/* deliberately left blank */};
	
	PersonVector::PersonVector(int capacity) : 
	              vectorSize(0),
	              vectorCapacity(0),
	              vector(NULL)
	{reserve(capacity);};
	
	PersonVector::PersonVector(const PersonVector& other) : 
	              vectorSize(0),
	              vectorCapacity(0),
	              vector(NULL)
	{
		reserve(other.vectorSize);
		for(int i=0; i < other.vectorSize; ++i)
			push_back(other.vector[i]);
	}
	
	PersonVector::~PersonVector() {
		delete [] vector;
	}

	void PersonVector::push_back(const Person& person) {
		if(vectorSize < vectorCapacity) {
            vector[vectorSize] = person;
        	++vectorSize;
        }
        else {
        	// Not enough memory for PersonVector.
        	// Trying to re-allocate memory;
        	increaseCapacity();
        	push_back(person);
        }
	}

	void PersonVector::pop_back() {
		if(vectorSize > 0)
            --vectorSize;
	}

	int PersonVector::size() const {
		return vectorSize;
	}
	
	int PersonVector::capacity() const {
		return vectorCapacity;
	}

	Person& PersonVector::at(unsigned int ind) {
		validateIndex(ind);
		return vector[ind];
	}
	
	const Person& PersonVector::at(unsigned int ind) const {
		validateIndex(ind);
		return vector[ind];
	}

	Person& PersonVector::operator[](unsigned int ind) {
		return at(ind);
	}
	
	const Person& PersonVector::operator[](unsigned int ind) const {
		return at(ind);
	}

	bool PersonVector::operator==(const PersonVector& other) const {
		if(vectorSize != other.vectorSize)
			return false;
		for(int i=0; i < vectorSize; ++i)
			if(vector[i] != other.vector[i])
				return false;
		return true;
	}
	
	bool PersonVector::operator!=(const PersonVector& other) const {
		return !(*this == other);
	}

	PersonVector& PersonVector::operator=(const PersonVector& other) {
		if(this != &other) {
			if(vectorCapacity < other.vectorSize) {
				remove();
				reserve(other.vectorSize);
			}
			clear();
			for(int i=0; i<other.vectorSize; ++i)
				push_back(other.vector[i]);
		}
		return *this;
	}

	bool PersonVector::empty(void) {
		return (vectorSize == 0);
	}

	void PersonVector::clear(void) {
		vectorSize = 0;
	}

	void PersonVector::remove(void) {
		vectorSize = 0;
		vectorCapacity = 0;
		delete [] vector;
	}

	void PersonVector::reserve(int capacity) {
		if(vectorCapacity == 0) {
            vector = new Person[capacity];
            vectorCapacity = capacity;
        }
        else {
            std::cout << "Cannot re-reserve memory for person.\n";
            std::exit(1);
        }
	}

	void PersonVector::increaseCapacity() {
		PersonVector temp(*this); //backup current vector
		remove();
		
		reserve(static_cast<int>(temp.capacity() * 1.5) + 5);
		*this = temp;
		// vectorSize = temp.size();
		// for(int i=0; i<temp.size(); ++i)
		// 	push_back(temp.vector[i]);
	}

	void PersonVector::validateIndex(int ind) const{
		if(ind > (vectorSize-1)) {
			std::cout << "Invalid index for PersonVector. Terminating.\n";
			std::exit(1);
		}
	}

}
