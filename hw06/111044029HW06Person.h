/*
 * File:    111044029HW06Person.h
 *
 * Course:  CSE241
 * Project: HW06
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef PERSON_H
#define	PERSON_H

#ifndef STRING_H
#include "111044029HW06String.h"
#endif

 #include <iostream>

namespace Tektas{

    class Person {
        public:
            // constructors
            Person();
            Person(String fName, String sName, bool sex, int birthYear, bool married = false);
            Person(const Person& other);

            //getters
            const String getFirstName() const;
            const String getLastName() const;
            const String getName() const; // first name + last name
            int getYearOfBirth() const;
            int getAge() const;
            bool isMale() const; // true for male, false for female
            bool isSingle() const;

            //setters
            void setFirstName(const String& fName);
            void setLastName(const String& fName);
            void setYearOfBirth(int birthYear);
            void setGender(bool gender);
            void setMarried();

            // operator overloadings
            Person& operator=(const Person& other);
            bool operator==(const Person& other) const;
            bool operator!=(const Person& other) const;
            // Person >> sample input: furkan,tektas,1991,male
            friend std::istream& operator>>(std::istream& stream, Person& obj);
            friend std::ostream& operator<<(std::ostream& stream, const Person& obj);
        private:
            Tektas::String firstName, lastName; // note that its not the std::String
            short int yearOfBirth; // 1900..2012
            bool gender; // true for male false for female
            bool single; // true for single false for married
    };
    namespace{
        int validateYearOfBirth(int year) {
            if ((year > 1950) && (year < 2012))
                return year;
            return 1970;
        }
    }
}

#endif	/* PERSON_H */
