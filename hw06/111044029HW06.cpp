/*
 * File:    111044029HW06.h
 *
 * Course:  CSE241
 * Project: HW06
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef FAMILY_H
#include "111044029HW06Family.h"
#endif

using namespace Tektas;

int Tektas::Family::familyCounter = 0;

bool isRelative(const Person& p1, const Person& p2,
    	  const Family** fams, int numOfFams);

int main(int argc, char const *argv[])
{
	
	Family simpsons("Marge,Simpson,female,1995,married","Homer,Simpson,male,1995,married"),
	       rubbles("Betty,Rubble,female,1960,married","Barney,Rubble,male,1960,married"),
	       flinstones("Vilma,Flinstone,female,1960,married","Fred,Flinstone,male,1960,married"),
	       jetsons("Jane,Jetson,female,2010,married","George,Jetson,male,2010,married"),
	       temp;

	// uncomment to test
	// std::cout << "Enter the mom's and dad's names as\n" 
	//          << "(name,surname,male/female,yearOfBirth,married/single)\n"
	//          << "(do not include any spaces and press return at the end"
	//          << "of mom's and dad's stream)\n";
	// std::cin >> temp;
	// std::cout << temp << std::endl;
	
    // Adding children for each families
	simpsons += Person("Lisa","Simpson",false,1998,true);
	simpsons += Person("Bart","Simpson",true,1999,true);
	simpsons += Person("Maggie","Simpson",false,2003,true);
	
	rubbles += Person("Bamm-Bamm","Rubble",true,1970,true);
	
	flinstones += Person("Pebbles","Flinstone",false,1970,true);
	
	jetsons += Person("Rudy","Jetson",false,2011,true);
	jetsons += Person("Elroy","Jetson",true,2012,true);

	// Marriage
	Family newFam = jetsons + flinstones;

	std::cout <<  simpsons << std::endl;
	std::cout <<  rubbles << std::endl;
	std::cout <<  flinstones << std::endl;
	std::cout <<  jetsons << std::endl;
	std::cout <<  newFam << std::endl;	
	
	{
		// living families and destructor test - works
		Family tektas(simpsons);
		std::cout << tektas << std::endl;
		std::cout << "Num of Living Families : "
		          << Family::getNumOfLivingFamilies() << std::endl;
	}	

	const Family *fams[] =  {&jetsons, &simpsons, &flinstones};
	int famNum = 3;

	std::cout << std::endl;
	std::cout << "Testing relatives : ";
	std::cout << std::boolalpha 
	          << isRelative(simpsons[2], simpsons[1],fams,famNum)
	          << std::endl << std::endl;

	std::cout << "Testing non-relatives : ";
	std::cout << std::boolalpha
	          << isRelative(simpsons[2], jetsons[1],fams,famNum)
	          << std::endl << std::endl;

	// note that tektas is not living anymore.
	std::cout << "Num of Living Families : "
		          << Family::getNumOfLivingFamilies() << std::endl;
	return 0;
}

bool isRelative(const Person& p1, const Person& p2,
    	  const Family** fams, int numOfFams) {
    	bool temp;
    	if(&p1 == &p2) 
    			return true; // same 
    	for(int i=0; i<numOfFams; ++i) {
    		temp = (p1 == fams[i]->getMom());
    		temp = ((p1 == fams[i]->getDad()) || temp);
    		for(int j=0;j<fams[i]->getNumOfChild();++j)
    			temp = ((p1 == fams[i]->getChild(j)) || temp);
    		if(temp) {
    			temp = (p2 == fams[i]->getMom());
	    		temp = ((p2 == fams[i]->getDad()) || temp);
	    		for(int j=0;j<fams[i]->getNumOfChild();++j)
	    			temp = ((p2 == fams[i]->getChild(j)) || temp);
	    		if(temp)
	    			return true;
    		}
    	}

    }
