/*
 * File:    111044029HW06Person.cpp
 *
 * Course:  CSE241
 * Project: HW06
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 11, 2012, 00:51 PM
 */

#ifndef SERIESOFPARTIALSUMS_H
#include "111044029HW06Person.h"
#include "111044029HW06PersonVector.h"
#endif

#ifndef STRING_H
#include "111044029HW06String.h"
#endif

namespace Tektas{
	Person::Person() : 
	               firstName(),
	               lastName(),
	               gender(true),
		           yearOfBirth(1970),
		           single(false)
		           {/* deliberately left blank */}

	Person::Person(String fName, String sName,
		           bool sex, int birthYear, bool married):
		           firstName(fName), lastName(sName), gender(sex),
		           yearOfBirth(validateYearOfBirth(birthYear)),
		           single(married)
		           {/* deliberately left blank */}

	Person::Person(const Person& other) :
	               firstName(other.firstName),
	               lastName(other.lastName),
	               yearOfBirth(other.yearOfBirth),
	               gender(other.gender),
	               single(other.single)
	               {/* deliberately left blank */}

	const String Person::getFirstName() const {
		return firstName;
	}

	const String Person::getLastName() const {
		return lastName;
	}

	const String Person::getName() const {
		return (firstName + " " + lastName);
	}

    int Person::getYearOfBirth() const {
    	return yearOfBirth;
    }

	int Person::getAge() const {
		return (2012 - yearOfBirth);
	}

    bool Person::isMale() const {
    	return gender;
    }
	
	bool Person::isSingle() const {
		return single;
	}

	void Person::setFirstName(const String& fName) {
		firstName = fName;
	}

	void Person::setLastName(const String& lName) {
		lastName = lName;
	}

	void Person::setYearOfBirth(int birthYear) {
		yearOfBirth = validateYearOfBirth(birthYear);
	}

	void Person::setGender(bool sex) {
		gender = sex;
	}

	void Person::setMarried() {
		single = false;
	}

	Person& Person::operator=(const Person& other) {
		if(this != &other)
		{
			firstName = other.firstName;
			lastName = other.lastName;
			gender = other.gender;
			yearOfBirth = other.yearOfBirth;
			single = other.single;
		}
		return *this;
	}

	bool Person::operator==(const Person& other) const {
		return ((firstName == other.firstName) && (lastName == other.lastName)
		        && (yearOfBirth == other.yearOfBirth) && (gender == other.gender)
		        && (single == other.single));
	}

	bool Person::operator!=(const Person& other) const {
		return !(*this == other);
	}
    std::ostream& operator<<(std::ostream& stream, const Person& obj) {
        stream << "Name            : " << obj.getName() << std::endl
               << "Year of birth   : " << obj.getYearOfBirth() << std::endl
               << "Maritial Status : ";
        
        if(obj.single)
        	stream << "Single\n";
        else
        	stream << "Married\n";
        	
        stream << "Gender          : ";

        if(obj.gender)
        	stream << "Male";
        else
        	stream << "Female";

    	stream << std::endl;
        return stream;
    }

    std::istream& operator>>(std::istream& stream, Person& obj) {
    	String input(80);
    	stream >> input;
    	obj.setFirstName(input.split(','));
    	obj.setLastName(input.split(',',1));
    	obj.setYearOfBirth(strToInt(input.split(',',2)));
    	obj.setGender((input.split(',',3) ==  "male"));
    }
}
