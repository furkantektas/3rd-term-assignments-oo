/*
 * File:    111044029HW04SeriesOfPartialSums.h
 *
 * Course:  CSE241
 * Project: HW04
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 1, 2012, 5:47 PM
 */

#ifndef SERIESOFPARTIALSUMS_H
#define	SERIESOFPARTIALSUMS_H


#include<vector> // for vectors

using std::vector;

class SeriesOfPartialSums {
public:

    SeriesOfPartialSums(int limit = 0, double term = 1.0/2.0);

    inline int getLimitK() const;

    inline double get(int index) const;
    // returns the index-th element of series

    static unsigned int getCounter();

    void setLimitK(int inpLimit);

    double evaluate () const;
    // evaluates the sum of all vector sum elements

    bool isSmaller(const SeriesOfPartialSums &otherSeries) const;
    // takes another series object as parameter and returns true
    // if the other series evaluates to a larger number

    void printElements() const;
    // prints the series elements to the screen

    friend void Test(SeriesOfPartialSums &sampleSeries);
    // friend function to test private function and access private data members


private:
    int limitK; // k
    double mainTerm; //an
    vector<double> sum; // all an's from an^0 to an^k
    static unsigned int counter; // SeriesOfPartalSums objects counter

    void add(double value);
    // pushes value to the end of the vector sum

    void remove();
    // removes an element from end of the vector sum

    void mult(double constant);
    // multiplies main term and refills the vector with new mainTerm

    bool validateK(int k)  const;
    // prompts user for invalid inputs and
    // terminates the program by exit code 1

    bool validateIndex(int ind) const;
    // returns true for valid index values

    inline bool isEmpty() const;
    // returns true if limit is zero

    void fillSeries();
    // fills the sum vector

    void printReduced(double num) const;
    // prints (double) num as a/b (eg: 0.5 -> 1/2)
    // requires: gcd()

    int gcd(int num1, int num2) const;
    // calculates the greatest common divisor

    static unsigned int increaseCounter();
    // increases the SeriesOfPartalSums objects counter
};
#endif	/* SERIESOFPARTIALSUMS_H */