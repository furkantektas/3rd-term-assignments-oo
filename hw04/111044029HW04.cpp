/*
 * File:    111044029HW04.cpp
 *
 * Course:  CSE241
 * Project: HW04
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 1, 2012, 5:47 PM
 */

#include<iostream>

#ifndef HW04_H
#include "111044029HW04.h"
#endif

using std::cout;

int main(int argc, char** argv) {
    const unsigned int ARRSIZE = 5;
    SeriesOfPartialSums sampleSeries(5), seriesArr[ARRSIZE];

    /* Testing class functions by friend function*/
    Test(sampleSeries);

    /* Testing class by different objects */
    testValue(sampleSeries);
    testReference(sampleSeries);
    testPointer(&sampleSeries);

    for(unsigned int i=0;i<ARRSIZE;++i)
        seriesArr[i].setLimitK(i+2);
    testArray(seriesArr);

    // printing defined num of SeriesOfPartialSums objects
    cout << SeriesOfPartialSums::getCounter() << " SeriesOfPartialSums objects "
         << "have been created.\n";
    return 0;
}


void Test(SeriesOfPartialSums &sampleSeries){
    // header
    for(unsigned int i=0;i<38;++i)
        cout << " ";
    cout << "TEST";
    for(unsigned int i=0;i<38;++i)
        cout << " ";
    cout << "\n";

    // horizontal line
    for(unsigned int i=0;i<80;++i)
        cout << "=";
    cout << "\n";

    cout << "Original: (Vector size " << sampleSeries.sum.size() << ")\n";
    sampleSeries.printElements();

    sampleSeries.remove();
    cout << "Removing the last element: "
         << "(Vector size " << sampleSeries.sum.size() << ")\n";
    sampleSeries.printElements();

    sampleSeries.add(9);
    cout << "Adding element 9 "
         << "(Vector size " << sampleSeries.sum.size() << ")\n";
    sampleSeries.printElements();

    cout << "evaluate() = " << sampleSeries.evaluate() << "\n";

    sampleSeries.mult(1.0/2.0);
    cout << "Multiplied by 1/2:\n";
    sampleSeries.printElements();

    cout << "evaluate() = " << sampleSeries.evaluate() << "\n";

    // horizontal line
    for(unsigned int i=0;i<80;++i)
        cout << "=";
    cout << "\n";
}
// This function tests different functions of this class.

void testReference(const SeriesOfPartialSums &refSeries)
{
    cout << "Testing call by reference by const reference\n";
    refSeries.printElements();
}

void testPointer(SeriesOfPartialSums *pointSeries)
{
    cout << "Testing call by simulated reference using pointers\n";
    pointSeries->printElements();
}

void testArray(SeriesOfPartialSums *arrSeries)
{
    cout << "Testing array of series object\n";
    arrSeries[0].printElements();
}

void testValue(SeriesOfPartialSums valueSeries)
{
    cout << "Testing call by value\n";
    valueSeries.printElements();
}