/*
 * File:    111044029HW04.h
 *
 * Course:  CSE241
 * Project: HW04
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 1, 2012, 5:47 PM
 */

#ifndef HW04_H
#define	HW04_H

#ifndef SERIESOFPARTIALSUMS_H
#include "111044029HW04SeriesOfPartialSums.h"
#endif

/* Test Functions */
void testReference(const SeriesOfPartialSums &refSeries);
void testPointer(SeriesOfPartialSums *pointSeries);
void testArray(SeriesOfPartialSums *arrSeries);
void testValue(SeriesOfPartialSums valueSeries);


#endif	/* HW04_H */

