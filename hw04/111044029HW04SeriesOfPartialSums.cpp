/*
 * File:    111044029HW04SeriesOfPartialSums.cpp
 *
 * Course:  CSE241
 * Project: HW04
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 * Created on November 1, 2012, 5:47 PM
 */

#ifndef SERIESOFPARTIALSUMS_H
#include "111044029HW04SeriesOfPartialSums.h"
#endif

#include<iostream> // for cout
#include<cstdlib> // for exit
#include<vector> // for vectors
#include<cmath> // for pow

using std::cout;
using std::exit;
using std::vector;
using std::pow;

SeriesOfPartialSums::SeriesOfPartialSums(int limit, double term) : mainTerm(term)
{
    setLimitK(limit);
    ++SeriesOfPartialSums::counter;
}

void SeriesOfPartialSums::setLimitK(int inpLimit) {
    if(validateK(inpLimit)) {
        limitK = inpLimit;
        // updating series
        fillSeries();
    }
}

inline int SeriesOfPartialSums::getLimitK() const {
    return limitK;
}

inline double SeriesOfPartialSums::get(int index) const {
    validateIndex(index);
    return sum[index];
}

void SeriesOfPartialSums::printElements() const {
    for(unsigned int i=0;i<sum.size();++i)
    {
        printReduced(sum[i]);
        cout << " ";
    }
    cout << "\n";
}

double SeriesOfPartialSums::evaluate () const
{
    double total = 0.0;
    if(!isEmpty())
        for(unsigned int i=0;i<sum.size();++i)
            total += sum[i];
    return total;
}

bool SeriesOfPartialSums::isSmaller(const SeriesOfPartialSums &otherSeries) const
{
    return (evaluate() < otherSeries.evaluate());
}

unsigned int SeriesOfPartialSums::getCounter(){
    return counter;
}

inline bool SeriesOfPartialSums::isEmpty() const {
    return (getLimitK() == 0);
}

void SeriesOfPartialSums::fillSeries() {
    for(unsigned int i=0;i<getLimitK();++i)
        add(pow(mainTerm,i));
}

void SeriesOfPartialSums::printReduced(double num) const {
    const int fractionLength = 100000;
    num *= fractionLength;
    const int commonDivisor = gcd(num, fractionLength);
    if(fractionLength/commonDivisor == 1)
        cout << num / commonDivisor << " ";
    else
        cout << num / commonDivisor << "/" << fractionLength / commonDivisor
             << " ";
}

int SeriesOfPartialSums::gcd(int num1, int num2) const{
    if(num2 == 0)
        return num1;
    gcd(num2, num1%num2);
}


void SeriesOfPartialSums::add(double value)
{
    sum.push_back(value);
}

void SeriesOfPartialSums::remove()
{
    if(sum.size() > 0)
        sum.pop_back();
}

void SeriesOfPartialSums::mult(double constant)
{
    for(unsigned int i=0; i<sum.size(); ++i)
        sum[i] *= constant;
}

bool SeriesOfPartialSums::validateK(int k)  const
{
    if(k < 0)
    {
        cout << "Invalid limit K. Terminating.\n";
        exit(1);
    }
    return true;
}

bool SeriesOfPartialSums::validateIndex(int ind) const
{
    return (ind >= 0);
}

// initializing counter by value 0
unsigned int SeriesOfPartialSums::counter = 0;
